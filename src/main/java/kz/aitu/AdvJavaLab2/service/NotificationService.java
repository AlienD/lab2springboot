package kz.aitu.AdvJavaLab2.service;

import kz.aitu.AdvJavaLab2.repository.NotificationRepository;
import org.springframework.http.ResponseEntity;
import org.springframework.stereotype.Service;

@Service
public class NotificationService {
    private final NotificationRepository notificationRepository;

    public NotificationService(NotificationRepository notificationRepository) {
        this.notificationRepository = notificationRepository;
    }

    public ResponseEntity<?> getAll(){
        return ResponseEntity.ok(notificationRepository.findAll());
    }

    public ResponseEntity<?> findByID(long id){
        return ResponseEntity.ok(notificationRepository.findById(id));
    }

    public void deleteByID(long id){
        notificationRepository.deleteById(id);
    }

    public void updateByID(long id, String objectType){
        notificationRepository.updateObjectTypeByID(objectType, id);
    }
}
