package kz.aitu.AdvJavaLab2.service;

import kz.aitu.AdvJavaLab2.repository.NomenclatureSummaryRepository;
import org.springframework.http.ResponseEntity;
import org.springframework.stereotype.Service;

@Service
public class NomenclatureSummaryService {
    private final NomenclatureSummaryRepository nomenclatureSummaryRepository;

    public NomenclatureSummaryService(NomenclatureSummaryRepository nomenclatureSummaryRepository) {
        this.nomenclatureSummaryRepository = nomenclatureSummaryRepository;
    }

    public ResponseEntity<?> getAll(){
        return ResponseEntity.ok(nomenclatureSummaryRepository.findAll());
    }

    public ResponseEntity<?> findByID(long id){
        return ResponseEntity.ok(nomenclatureSummaryRepository.findById(id));
    }

    public void deleteByID(long id){
        nomenclatureSummaryRepository.deleteById(id);
    }

    public void updateByID(long id, int year){
        nomenclatureSummaryRepository.updateYearByID(year, id);
    }
}
