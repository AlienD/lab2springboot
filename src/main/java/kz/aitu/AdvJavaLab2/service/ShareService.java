package kz.aitu.AdvJavaLab2.service;

import kz.aitu.AdvJavaLab2.repository.ShareRepository;
import org.springframework.http.ResponseEntity;
import org.springframework.stereotype.Service;

@Service
public class ShareService {
    private final ShareRepository shareRepository;

    public ShareService(ShareRepository shareRepository) {
        this.shareRepository = shareRepository;
    }

    public ResponseEntity<?> getAll(){
        return ResponseEntity.ok(shareRepository.findAll());
    }

    public ResponseEntity<?> findByID(long id){
        return ResponseEntity.ok(shareRepository.findById(id));
    }

    public void deleteByID(long id){
        shareRepository.deleteById(id);
    }

    public void updateByID(long id, String note){
       shareRepository.updateNoteByID(note, id);
    }
}
