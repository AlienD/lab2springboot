package kz.aitu.AdvJavaLab2.service;

import kz.aitu.AdvJavaLab2.repository.CompanyUnitRepository;
import org.springframework.http.ResponseEntity;
import org.springframework.stereotype.Service;

@Service
public class CompanyUnitService {
    private final CompanyUnitRepository companyUnitRepository;

    public CompanyUnitService(CompanyUnitRepository companyUnitRepository) {
        this.companyUnitRepository = companyUnitRepository;
    }

    public ResponseEntity<?> getAll(){
        return ResponseEntity.ok(companyUnitRepository.findAll());
    }

    public ResponseEntity<?> findByID(long id){
        return ResponseEntity.ok(companyUnitRepository.findById(id));
    }

    public void deleteByID(long id){
        companyUnitRepository.deleteById(id);
    }

    public void updateByID(long id, String nameEN){
        companyUnitRepository.updateNameEnByID(nameEN, id);
    }
}
