package kz.aitu.AdvJavaLab2.service;

import kz.aitu.AdvJavaLab2.repository.UsersRepository;
import org.springframework.http.ResponseEntity;
import org.springframework.stereotype.Service;

@Service
public class UsersService {
    private final UsersRepository usersRepository;

    public UsersService(UsersRepository usersRepository) {
        this.usersRepository = usersRepository;
    }

    public ResponseEntity<?> getAll(){
        return ResponseEntity.ok(usersRepository.findAll());
    }

    public ResponseEntity<?> findByID(long id){
        return ResponseEntity.ok(usersRepository.findById(id));
    }

    public void deleteByID(long id){
        usersRepository.deleteById(id);
    }

    public void updateByID(long id, String name){
        usersRepository.updateNameByID(name,id);
    }
}
