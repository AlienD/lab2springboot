package kz.aitu.AdvJavaLab2.service;

import kz.aitu.AdvJavaLab2.repository.CatalogRepository;
import org.springframework.http.ResponseEntity;
import org.springframework.stereotype.Service;

@Service
public class CatalogService {
    private final CatalogRepository catalogRepository;

    public CatalogService(CatalogRepository catalogRepository) {
        this.catalogRepository = catalogRepository;
    }

    public ResponseEntity<?> getAll(){
        return ResponseEntity.ok(catalogRepository.findAll());
    }

    public ResponseEntity<?> findByID(long id){
        return ResponseEntity.ok(catalogRepository.findById(id));
    }

    public void deleteByID(long id){
        catalogRepository.deleteById(id);
    }

    public void updateByID(long id, String nameEN){
        catalogRepository.updateNameEnByID(nameEN, id);
    }
}
