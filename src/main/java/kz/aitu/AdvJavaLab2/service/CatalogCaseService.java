package kz.aitu.AdvJavaLab2.service;

import kz.aitu.AdvJavaLab2.repository.CatalogCaseRepository;
import org.springframework.http.ResponseEntity;
import org.springframework.stereotype.Service;

@Service
public class CatalogCaseService {
    private final CatalogCaseRepository catalogCaseRepository;

    public CatalogCaseService(CatalogCaseRepository catalogCaseRepository) {
        this.catalogCaseRepository = catalogCaseRepository;
    }

    public ResponseEntity<?> getAll(){
        return ResponseEntity.ok(catalogCaseRepository.findAll());
    }

    public ResponseEntity<?> findByID(long id){
        return ResponseEntity.ok(catalogCaseRepository.findById(id));
    }

    public void deleteByID(long id){
        catalogCaseRepository.deleteById(id);
    }

    public void updateByID(long id, long createdBy){
        catalogCaseRepository.updateCreatedByByID(createdBy, id);
    }
}
