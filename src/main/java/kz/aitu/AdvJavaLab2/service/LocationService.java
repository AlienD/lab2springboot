package kz.aitu.AdvJavaLab2.service;

import kz.aitu.AdvJavaLab2.repository.LocationRepository;
import org.springframework.http.ResponseEntity;
import org.springframework.stereotype.Service;

@Service
public class LocationService {
    private final LocationRepository locationRepository;

    public LocationService(LocationRepository locationRepository) {
        this.locationRepository = locationRepository;
    }

    public ResponseEntity<?> getAll(){
        return ResponseEntity.ok(locationRepository.findAll());
    }

    public ResponseEntity<?> findByID(long id){
        return ResponseEntity.ok(locationRepository.findById(id));
    }

    public void deleteByID(long id){
        locationRepository.deleteById(id);
    }

    public void updateByID(long id, String box){
        locationRepository.updateBoxByID(box, id);
    }
}
