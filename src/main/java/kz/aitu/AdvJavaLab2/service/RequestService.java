package kz.aitu.AdvJavaLab2.service;

import kz.aitu.AdvJavaLab2.repository.RequestRepository;
import org.springframework.http.ResponseEntity;
import org.springframework.stereotype.Service;

@Service
public class RequestService {
    private final RequestRepository requestRepository;

    public RequestService(RequestRepository requestRepository) {
        this.requestRepository = requestRepository;
    }

    public ResponseEntity<?> getAll(){
        return ResponseEntity.ok(requestRepository.findAll());
    }

    public ResponseEntity<?> findByID(long id){
        return ResponseEntity.ok(requestRepository.findById(id));
    }

    public void deleteByID(long id){
        requestRepository.deleteById(id);
    }

    public void updateByID(long id, long requestUserId){
        requestRepository.updateRequestUserIdByID(requestUserId, id);
    }
}
