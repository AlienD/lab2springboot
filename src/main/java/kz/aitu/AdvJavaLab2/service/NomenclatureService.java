package kz.aitu.AdvJavaLab2.service;

import kz.aitu.AdvJavaLab2.repository.NomenclatureRepository;
import org.springframework.http.ResponseEntity;
import org.springframework.stereotype.Service;

@Service
public class NomenclatureService {
    private final NomenclatureRepository nomenclatureRepository;

    public NomenclatureService(NomenclatureRepository nomenclatureRepository) {
        this.nomenclatureRepository = nomenclatureRepository;
    }

    public ResponseEntity<?> getAll(){
        return ResponseEntity.ok(nomenclatureRepository.findAll());
    }

    public ResponseEntity<?> findByID(long id){
        return ResponseEntity.ok(nomenclatureRepository.findById(id));
    }

    public void deleteByID(long id){
        nomenclatureRepository.deleteById(id);
    }

    public void updateByID(long id, int year){
        nomenclatureRepository.updateYearByID(year, id);
    }
}
