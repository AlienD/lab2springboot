package kz.aitu.AdvJavaLab2.service;

import kz.aitu.AdvJavaLab2.repository.SearchKeyRepository;
import org.springframework.http.ResponseEntity;
import org.springframework.stereotype.Service;

@Service
public class SearchKeyService {
    private final SearchKeyRepository searchKeyRepository;

    public SearchKeyService(SearchKeyRepository searchKeyRepository) {
        this.searchKeyRepository = searchKeyRepository;
    }

    public ResponseEntity<?> getAll(){
        return ResponseEntity.ok(searchKeyRepository.findAll());
    }

    public ResponseEntity<?> findByID(long id){
        return ResponseEntity.ok(searchKeyRepository.findById(id));
    }

    public void deleteByID(long id){
        searchKeyRepository.deleteById(id);
    }

    public void updateByID(long id, String name){
        searchKeyRepository.updateNameByID(name, id);
    }
}
