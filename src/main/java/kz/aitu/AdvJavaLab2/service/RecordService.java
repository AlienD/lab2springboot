package kz.aitu.AdvJavaLab2.service;

import kz.aitu.AdvJavaLab2.repository.RecordRepository;
import org.springframework.http.ResponseEntity;
import org.springframework.stereotype.Service;

@Service
public class RecordService {
    private final RecordRepository recordRepository;

    public RecordService(RecordRepository recordRepository) {
        this.recordRepository = recordRepository;
    }

    public ResponseEntity<?> getAll(){
        return ResponseEntity.ok(recordRepository.findAll());
    }

    public ResponseEntity<?> findByID(long id){
        return ResponseEntity.ok(recordRepository.findById(id));
    }

    public void deleteByID(long id){
        recordRepository.deleteById(id);
    }

    public void updateByID(long id, String number){
        recordRepository.updateNumberByID(number, id);
    }
}
