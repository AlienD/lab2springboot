package kz.aitu.AdvJavaLab2.service;

import kz.aitu.AdvJavaLab2.repository.TempFilesRepository;
import org.springframework.http.ResponseEntity;
import org.springframework.stereotype.Service;

@Service
public class TempFilesService {
    private final TempFilesRepository tempFilesRepository;

    public TempFilesService(TempFilesRepository tempFilesRepository) {
        this.tempFilesRepository = tempFilesRepository;
    }

    public ResponseEntity<?> getAll(){
        return ResponseEntity.ok(tempFilesRepository.findAll());
    }

    public ResponseEntity<?> findByID(long id){
        return ResponseEntity.ok(tempFilesRepository.findById(id));
    }

    public void deleteByID(long id){
        tempFilesRepository.deleteById(id);
    }

    public void updateByID(long id, short fileBinaryByte){
        tempFilesRepository.updateFileBinaryByteByID(fileBinaryByte, id);
    }
}
