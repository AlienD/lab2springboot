package kz.aitu.AdvJavaLab2.service;

import kz.aitu.AdvJavaLab2.repository.FondRepository;
import org.springframework.http.ResponseEntity;
import org.springframework.stereotype.Service;

@Service
public class FondService {
    private final FondRepository fondRepository;

    public FondService(FondRepository fondRepository) {
        this.fondRepository = fondRepository;
    }

    public ResponseEntity<?> getAll(){
        return ResponseEntity.ok(fondRepository.findAll());
    }

    public ResponseEntity<?> findByID(long id){
        return ResponseEntity.ok(fondRepository.findById(id));
    }

    public void deleteByID(long id){
        fondRepository.deleteById(id);
    }

    public void updateByID(long id, String fondNumber){
        fondRepository.updateFondNumberByID(fondNumber, id);
    }
}
