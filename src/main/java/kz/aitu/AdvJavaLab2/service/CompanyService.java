package kz.aitu.AdvJavaLab2.service;

import kz.aitu.AdvJavaLab2.repository.CompanyRepository;
import org.springframework.http.ResponseEntity;
import org.springframework.stereotype.Service;

@Service
public class CompanyService {
    private final CompanyRepository companyRepository;

    public CompanyService(CompanyRepository companyRepository) {
        this.companyRepository = companyRepository;
    }

    public ResponseEntity<?> getAll(){
        return ResponseEntity.ok(companyRepository.findAll());
    }

    public ResponseEntity<?> findByID(long id){
        return ResponseEntity.ok(companyRepository.findById(id));
    }

    public void deleteByID(long id){
        companyRepository.deleteById(id);
    }

    public void updateByID(long id, String nameEN){
        companyRepository.updateNameEnByID(nameEN, id);
    }
}
