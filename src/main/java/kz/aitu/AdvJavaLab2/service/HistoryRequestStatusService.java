package kz.aitu.AdvJavaLab2.service;

import kz.aitu.AdvJavaLab2.repository.HistoryRequestStatusRepository;
import org.springframework.http.ResponseEntity;
import org.springframework.stereotype.Service;

@Service
public class HistoryRequestStatusService {
    private final HistoryRequestStatusRepository historyRequestStatusRepository;

    public HistoryRequestStatusService(HistoryRequestStatusRepository historyRequestStatusRepository) {
        this.historyRequestStatusRepository = historyRequestStatusRepository;
    }

    public ResponseEntity<?> getAll(){
        return ResponseEntity.ok(historyRequestStatusRepository.findAll());
    }

    public ResponseEntity<?> findByID(long id){
        return ResponseEntity.ok(historyRequestStatusRepository.findById(id));
    }

    public void deleteByID(long id){
        historyRequestStatusRepository.deleteById(id);
    }

    public void updateByID(long id, String status){
        historyRequestStatusRepository.updateStatusByID(status, id);
    }
}
