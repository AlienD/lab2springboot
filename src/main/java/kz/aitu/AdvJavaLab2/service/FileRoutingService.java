package kz.aitu.AdvJavaLab2.service;

import kz.aitu.AdvJavaLab2.repository.FileRoutingRepository;
import org.springframework.http.ResponseEntity;
import org.springframework.stereotype.Service;

@Service
public class FileRoutingService {
    private final FileRoutingRepository fileRoutingRepository;

    public FileRoutingService(FileRoutingRepository fileRoutingRepository) {
        this.fileRoutingRepository = fileRoutingRepository;
    }

    public ResponseEntity<?> getAll(){
        return ResponseEntity.ok(fileRoutingRepository.findAll());
    }

    public ResponseEntity<?> findByID(long id){
        return ResponseEntity.ok(fileRoutingRepository.findById(id));
    }

    public void deleteByID(long id){
        fileRoutingRepository.deleteById(id);
    }

    public void updateByID(long id, String tableName){
        fileRoutingRepository.updateTableNameByID(tableName,id);
    }
}
