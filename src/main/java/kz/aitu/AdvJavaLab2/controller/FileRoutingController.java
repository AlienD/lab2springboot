package kz.aitu.AdvJavaLab2.controller;

import kz.aitu.AdvJavaLab2.service.FileRoutingService;
import org.springframework.http.ResponseEntity;
import org.springframework.web.bind.annotation.*;

@RestController
public class FileRoutingController {
    private final FileRoutingService fileRoutingService;

    public FileRoutingController(FileRoutingService fileRoutingService) {
        this.fileRoutingService = fileRoutingService;
    }

    @GetMapping("/lab2/fileRoutingServices")
    public ResponseEntity<?> getFileRoutingServices() {
        return ResponseEntity.ok(fileRoutingService.getAll());
    }

    @GetMapping("/lab2/fileRoutingService/{id}")
    public ResponseEntity<?> findFileRoutingServiceByID(@PathVariable long id){
        return ResponseEntity.ok(fileRoutingService.findByID(id));
    }

    @DeleteMapping("/lab2/deleteFileRoutingService/{id}")
    public void deleteFileRoutingServiceByID(@PathVariable long id){
        fileRoutingService.deleteByID(id);
    }

    @RequestMapping(value = "/lab2/updateFileRoutingService/{id}/{tableName}", method = RequestMethod.GET)
    public void updateFileRoutingServiceByID(@PathVariable("id") long id, @PathVariable("tableName") String tableName){
        fileRoutingService.updateByID(id, tableName);
    }
}
