package kz.aitu.AdvJavaLab2.controller;

import kz.aitu.AdvJavaLab2.service.NomenclatureService;
import org.springframework.http.ResponseEntity;
import org.springframework.web.bind.annotation.*;

@RestController
public class NomenclatureController {
    private final NomenclatureService nomenclatureService;

    public NomenclatureController(NomenclatureService nomenclatureService) {
        this.nomenclatureService = nomenclatureService;
    }

    @GetMapping("/lab2/nomenclatures")
    public ResponseEntity<?> getNomenclatures() {
        return ResponseEntity.ok(nomenclatureService.getAll());
    }

    @GetMapping("/lab2/nomenclature/{id}")
    public ResponseEntity<?> findNomenclatureByID(@PathVariable long id){
        return ResponseEntity.ok(nomenclatureService.findByID(id));
    }

    @DeleteMapping("/lab2/deleteNomenclature/{id}")
    public void deleteNomenclatureByID(@PathVariable long id){
        nomenclatureService.deleteByID(id);
    }

    @RequestMapping(value = "/lab2/updateNomenclature/{id}/{year}", method = RequestMethod.GET)
    public void updateNomenclatureByID(@PathVariable("id") long id, @PathVariable("year") int year){
        nomenclatureService.updateByID(id, year);
    }
}
