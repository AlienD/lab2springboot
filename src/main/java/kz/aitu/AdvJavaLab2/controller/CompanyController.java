package kz.aitu.AdvJavaLab2.controller;

import kz.aitu.AdvJavaLab2.service.CompanyService;
import org.springframework.http.ResponseEntity;
import org.springframework.web.bind.annotation.*;

@RestController
public class CompanyController {
    private final CompanyService companyService;

    public CompanyController(CompanyService companyService) {
        this.companyService = companyService;
    }

    @GetMapping("/lab2/companies")
    public ResponseEntity<?> getCompanies() {
        return ResponseEntity.ok(companyService.getAll());
    }

    @GetMapping("/lab2/company/{id}")
    public ResponseEntity<?> findCompanyByID(@PathVariable long id){
        return ResponseEntity.ok(companyService.findByID(id));
    }

    @DeleteMapping("/lab2/deleteCompany/{id}")
    public void deleteCompanyByID(@PathVariable long id){
        companyService.deleteByID(id);
    }

    @RequestMapping(value = "/lab2/updateCompany/{id}/{nameEN}", method = RequestMethod.GET)
    public void updateCompanyByID(@PathVariable("id") long id, @PathVariable("titleEN") String nameEN){
        companyService.updateByID(id, nameEN);
    }
}
