package kz.aitu.AdvJavaLab2.controller;

import kz.aitu.AdvJavaLab2.service.CaseIndexService;
import org.springframework.http.ResponseEntity;
import org.springframework.web.bind.annotation.*;

@RestController
public class CaseIndexController {
    private final CaseIndexService caseIndexService;

    public CaseIndexController(CaseIndexService caseIndexService) {
        this.caseIndexService = caseIndexService;
    }

    @GetMapping("/lab2/caseIndexes")
    public ResponseEntity<?> getCaseIndexes() {
        return ResponseEntity.ok(caseIndexService.getAll());
    }

    @GetMapping("/lab2/caseIndex/{id}")
    public ResponseEntity<?> findCaseIndexByID(@PathVariable long id){
        return ResponseEntity.ok(caseIndexService.findByID(id));
    }

    @DeleteMapping("/lab2/deleteCaseIndex/{id}")
    public void deleteCaseIndexByID(@PathVariable long id){
        caseIndexService.deleteByID(id);
    }

    @RequestMapping(value = "/lab2/updateCaseIndex/{id}/{titleEN}", method = RequestMethod.GET)
    public void updateCaseByID(@PathVariable("id") long id, @PathVariable("titleEN") String titleEN){
        caseIndexService.updateByID(id, titleEN);
    }
}
