package kz.aitu.AdvJavaLab2.controller;

import kz.aitu.AdvJavaLab2.service.NomenclatureSummaryService;
import org.springframework.http.ResponseEntity;
import org.springframework.web.bind.annotation.*;

@RestController
public class NomenclatureSummaryController {
    private final NomenclatureSummaryService nomenclatureSummaryService;

    public NomenclatureSummaryController(NomenclatureSummaryService nomenclatureSummaryService) {
        this.nomenclatureSummaryService = nomenclatureSummaryService;
    }

    @GetMapping("/lab2/nomenclatureSummaries")
    public ResponseEntity<?> getNomenclatureSummaries() {
        return ResponseEntity.ok(nomenclatureSummaryService.getAll());
    }

    @GetMapping("/lab2/nomenclatureSummary/{id}")
    public ResponseEntity<?> findNomenclatureSummaryByID(@PathVariable long id){
        return ResponseEntity.ok(nomenclatureSummaryService.findByID(id));
    }

    @DeleteMapping("/lab2/deleteNomenclatureSummary/{id}")
    public void deleteNomenclatureSummaryByID(@PathVariable long id){
        nomenclatureSummaryService.deleteByID(id);
    }

    @RequestMapping(value = "/lab2/updateNomenclatureSummary/{id}/{year}", method = RequestMethod.GET)
    public void updateNomenclatureSummaryByID(@PathVariable("id") long id, @PathVariable("year") int year){
        nomenclatureSummaryService.updateByID(id, year);
    }
}
