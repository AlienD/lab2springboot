package kz.aitu.AdvJavaLab2.controller;

import kz.aitu.AdvJavaLab2.service.SearchKeyService;
import org.springframework.http.ResponseEntity;
import org.springframework.web.bind.annotation.*;

@RestController
public class SearchKeyController {
    private final SearchKeyService searchKeyService;

    public SearchKeyController(SearchKeyService searchKeyService) {
        this.searchKeyService = searchKeyService;
    }

    @GetMapping("/lab2/searchKeys")
    public ResponseEntity<?> getSearchKeys() {
        return ResponseEntity.ok(searchKeyService.getAll());
    }

    @GetMapping("/lab2/searchKey/{id}")
    public ResponseEntity<?> findSearchKeyByID(@PathVariable long id){
        return ResponseEntity.ok(searchKeyService.findByID(id));
    }

    @DeleteMapping("/lab2/deleteSearchKey/{id}")
    public void deleteSearchKeyByID(@PathVariable long id){
        searchKeyService.deleteByID(id);
    }

    @RequestMapping(value = "/lab2/updateSearchKey/{id}/{name}", method = RequestMethod.GET)
    public void updateSearchKeyByID(@PathVariable("id") long id, @PathVariable("name") String name){
        searchKeyService.updateByID(id, name);
    }
}
