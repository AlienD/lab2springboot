package kz.aitu.AdvJavaLab2.controller;

import kz.aitu.AdvJavaLab2.service.FileService;
import org.springframework.http.ResponseEntity;
import org.springframework.web.bind.annotation.*;

@RestController
public class FileController {
    private final FileService fileService;

    public FileController(FileService fileService) {
        this.fileService = fileService;
    }

    @GetMapping("/lab2/fileServices")
    public ResponseEntity<?> getFileServices() {
        return ResponseEntity.ok(fileService.getAll());
    }

    @GetMapping("/lab2/fileService/{id}")
    public ResponseEntity<?> findFileServiceByID(@PathVariable long id){
        return ResponseEntity.ok(fileService.findByID(id));
    }

    @DeleteMapping("/lab2/deleteFileService/{id}")
    public void deleteFileServiceByID(@PathVariable long id){
        fileService.deleteByID(id);
    }

    @RequestMapping(value = "/lab2/updateFileService/{id}/{name}", method = RequestMethod.GET)
    public void updateFileServiceByID(@PathVariable("id") long id, @PathVariable("name") String name){
        fileService.updateByID(id, name);
    }
}
