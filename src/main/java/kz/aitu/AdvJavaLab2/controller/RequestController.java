package kz.aitu.AdvJavaLab2.controller;

import kz.aitu.AdvJavaLab2.service.RequestService;
import org.springframework.http.ResponseEntity;
import org.springframework.web.bind.annotation.*;

@RestController
public class RequestController {
    private final RequestService requestService;

    public RequestController(RequestService requestService) {
        this.requestService = requestService;
    }

    @GetMapping("/lab2/requests")
    public ResponseEntity<?> getRequests() {
        return ResponseEntity.ok(requestService.getAll());
    }

    @GetMapping("/lab2/request/{id}")
    public ResponseEntity<?> findRequestByID(@PathVariable long id){
        return ResponseEntity.ok(requestService.findByID(id));
    }

    @DeleteMapping("/lab2/deleteRequest/{id}")
    public void deleteRequestByID(@PathVariable long id){
        requestService.deleteByID(id);
    }

    @RequestMapping(value = "/lab2/updateRequest/{id}/{requestUserId}", method = RequestMethod.GET)
    public void updateRequestByID(@PathVariable("id") long id, @PathVariable("requestUserId") long requestUserId){
        requestService.updateByID(id, requestUserId);
    }
}
