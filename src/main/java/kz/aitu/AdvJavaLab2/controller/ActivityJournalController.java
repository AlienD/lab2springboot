package kz.aitu.AdvJavaLab2.controller;

import kz.aitu.AdvJavaLab2.model.ActivityJournal;
import kz.aitu.AdvJavaLab2.repository.ActivityJournalRepository;
import kz.aitu.AdvJavaLab2.service.ActivityJournalService;
import org.springframework.http.ResponseEntity;
import org.springframework.web.bind.annotation.*;

@RestController
public class ActivityJournalController {
    private final ActivityJournalService activityJournalService;

    public ActivityJournalController(ActivityJournalService activityJournalService) {
        this.activityJournalService = activityJournalService;
    }

    @GetMapping("/lab2/activityJournals")
    public ResponseEntity<?> getActivityJournals(){
        return activityJournalService.getActivityJournals();
    }

    @GetMapping("/lab2/activityJournal/{id}")
    public ResponseEntity<?> findActivityJournalByID(@PathVariable long id){
        return activityJournalService.findActivityJournalByID(id);
    }

    @DeleteMapping("/lab2/deleteActivityJournal/{id}")
    public void deleteActivityJournalByID(@PathVariable long id){
        activityJournalService.deleteActivityJournalByID(id);
    }

    @RequestMapping(value = "/lab2/updateActivityJournal/{id}/{eventType}", method = RequestMethod.GET)
    public void updateActivityJournalByID(@PathVariable("id") long id, @PathVariable("eventType") String eventType){
        activityJournalService.updateActivityJournalByID(id,eventType);
    }

    @PostMapping("/lab2/ActJour")
    public ResponseEntity<?> createActivityJournal(@RequestBody ActivityJournal activityJournal){
        return ResponseEntity.ok(activityJournalService.create(activityJournal));
    }

}
