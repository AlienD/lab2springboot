package kz.aitu.AdvJavaLab2.controller;

import kz.aitu.AdvJavaLab2.service.RecordService;
import org.springframework.http.ResponseEntity;
import org.springframework.web.bind.annotation.*;

@RestController
public class RecordController {
    private final RecordService recordService;

    public RecordController(RecordService recordService) {
        this.recordService = recordService;
    }

    @GetMapping("/lab2/records")
    public ResponseEntity<?> getRecords() {
        return ResponseEntity.ok(recordService.getAll());
    }

    @GetMapping("/lab2/record/{id}")
    public ResponseEntity<?> findRecordByID(@PathVariable long id){
        return ResponseEntity.ok(recordService.findByID(id));
    }

    @DeleteMapping("/lab2/deleteRecord/{id}")
    public void deleteRecordByID(@PathVariable long id){
        recordService.deleteByID(id);
    }

    @RequestMapping(value = "/lab2/updateRecord/{id}/{number}", method = RequestMethod.GET)
    public void updateRecordByID(@PathVariable("id") long id, @PathVariable("number") String number){
        recordService.updateByID(id, number);
    }
}
