package kz.aitu.AdvJavaLab2.controller;

import kz.aitu.AdvJavaLab2.service.DestructionActService;
import org.springframework.http.ResponseEntity;
import org.springframework.web.bind.annotation.*;

@RestController
public class DestructionActController {
    private final DestructionActService destructionActService;

    public DestructionActController(DestructionActService destructionActService) {
        this.destructionActService = destructionActService;
    }

    @GetMapping("/lab2/destructionActs")
    public ResponseEntity<?> getDestructionActs() {
        return ResponseEntity.ok(destructionActService.getAll());
    }

    @GetMapping("/lab2/destructionAct/{id}")
    public ResponseEntity<?> findDestructionActByID(@PathVariable long id){
        return ResponseEntity.ok(destructionActService.findByID(id));
    }

    @DeleteMapping("/lab2/deleteDestructionAct/{id}")
    public void deleteDestructionActByID(@PathVariable long id){
        destructionActService.deleteByID(id);
    }

    @RequestMapping(value = "/lab2/updateDestructionAct/{id}/{base}", method = RequestMethod.GET)
    public void updateDestructionActByID(@PathVariable("id") long id, @PathVariable("base") String base){
        destructionActService.updateByID(id, base);
    }
}
