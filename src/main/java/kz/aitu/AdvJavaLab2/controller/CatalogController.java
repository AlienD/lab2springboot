package kz.aitu.AdvJavaLab2.controller;

import kz.aitu.AdvJavaLab2.service.CatalogService;
import org.springframework.http.ResponseEntity;
import org.springframework.web.bind.annotation.*;

@RestController
public class CatalogController {
    private final CatalogService catalogService;

    public CatalogController(CatalogService catalogService) {
        this.catalogService = catalogService;
    }

    @GetMapping("/lab2/catalogs")
    public ResponseEntity<?> getCatalogs() {
        return ResponseEntity.ok(catalogService.getAll());
    }

    @GetMapping("/lab2/catalog/{id}")
    public ResponseEntity<?> findCatalogByID(@PathVariable long id){
        return ResponseEntity.ok(catalogService.findByID(id));
    }

    @DeleteMapping("/lab2/deleteCatalog/{id}")
    public void deleteCatalogByID(@PathVariable long id){
        catalogService.deleteByID(id);
    }

    @RequestMapping(value = "/lab2/updateCatalog/{id}/{nameEN}", method = RequestMethod.GET)
    public void updateCatalogByID(@PathVariable("id") long id, @PathVariable("nameEN") String nameEN){
        catalogService.updateByID(id, nameEN);
    }
}
