package kz.aitu.AdvJavaLab2.controller;

import kz.aitu.AdvJavaLab2.service.HistoryRequestStatusService;
import org.springframework.http.ResponseEntity;
import org.springframework.web.bind.annotation.*;

@RestController
public class HistoryRequestStatusController {
    private final HistoryRequestStatusService historyRequestStatusService;

    public HistoryRequestStatusController(HistoryRequestStatusService historyRequestStatusService) {
        this.historyRequestStatusService = historyRequestStatusService;
    }

    @GetMapping("/lab2/historyRequestStatuses")
    public ResponseEntity<?> getHistoryRequestStatuses() {
        return ResponseEntity.ok(historyRequestStatusService.getAll());
    }

    @GetMapping("/lab2/historyRequestStatus/{id}")
    public ResponseEntity<?> findHistoryRequestStatusByID(@PathVariable long id){
        return ResponseEntity.ok(historyRequestStatusService.findByID(id));
    }

    @DeleteMapping("/lab2/deleteHistoryRequestStatus/{id}")
    public void deleteHistoryRequestStatusByID(@PathVariable long id){
        historyRequestStatusService.deleteByID(id);
    }

    @RequestMapping(value = "/lab2/updateHistoryRequestStatus/{id}/{status}", method = RequestMethod.GET)
    public void updateHistoryRequestStatusByID(@PathVariable("id") long id, @PathVariable("status") String status){
        historyRequestStatusService.updateByID(id, status);
    }
}
