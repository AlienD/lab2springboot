package kz.aitu.AdvJavaLab2.controller;

import kz.aitu.AdvJavaLab2.service.UsersService;
import org.springframework.http.ResponseEntity;
import org.springframework.web.bind.annotation.*;

@RestController
public class UsersController {
    private final UsersService usersService;

    public UsersController(UsersService usersService) {
        this.usersService = usersService;
    }

    @GetMapping("/lab2/users")
    public ResponseEntity<?> getUsers() {
        return ResponseEntity.ok(usersService.getAll());
    }

    @GetMapping("/lab2/user/{id}")
    public ResponseEntity<?> findUserByID(@PathVariable long id){
        return ResponseEntity.ok(usersService.findByID(id));
    }

    @DeleteMapping("/lab2/deleteUser/{id}")
    public void deleteUserByID(@PathVariable long id){
        usersService.deleteByID(id);
    }

    @RequestMapping(value = "/lab2/updateUser/{id}/{name}", method = RequestMethod.GET)
    public void updateUserByID(@PathVariable("id") long id, @PathVariable("name") String name){
        usersService.updateByID(id, name);
    }
}
