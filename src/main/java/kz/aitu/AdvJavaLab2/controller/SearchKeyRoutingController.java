package kz.aitu.AdvJavaLab2.controller;

import kz.aitu.AdvJavaLab2.service.SearchKeyRoutingService;
import kz.aitu.AdvJavaLab2.service.SearchKeyService;
import org.springframework.http.ResponseEntity;
import org.springframework.web.bind.annotation.*;

@RestController
public class SearchKeyRoutingController {
    private final SearchKeyRoutingService searchKeyRoutingService;

    public SearchKeyRoutingController(SearchKeyRoutingService searchKeyRoutingService) {
        this.searchKeyRoutingService = searchKeyRoutingService;
    }

    @GetMapping("/lab2/searchKeyRoutings")
    public ResponseEntity<?> getSearchKeyRoutings() {
        return ResponseEntity.ok(searchKeyRoutingService.getAll());
    }

    @GetMapping("/lab2/searchKeyRouting/{id}")
    public ResponseEntity<?> findSearchKeyRoutingByID(@PathVariable long id){
        return ResponseEntity.ok(searchKeyRoutingService.findByID(id));
    }

    @DeleteMapping("/lab2/deleteSearchKeyRouting/{id}")
    public void deleteSearchKeyRoutingByID(@PathVariable long id){
        searchKeyRoutingService.deleteByID(id);
    }

    @RequestMapping(value = "/lab2/updateSearchKeyRouting/{id}/{tableName}", method = RequestMethod.GET)
    public void updateSearchKeyRoutingByID(@PathVariable("id") long id, @PathVariable("tableName") String tableName){
        searchKeyRoutingService.updateByID(id, tableName);
    }
}
