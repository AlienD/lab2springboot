package kz.aitu.AdvJavaLab2.controller;

import kz.aitu.AdvJavaLab2.repository.ActivityJournalRepository;
import kz.aitu.AdvJavaLab2.repository.AuthorizationNRepository;
import kz.aitu.AdvJavaLab2.service.AuthorizationNService;
import org.springframework.http.ResponseEntity;
import org.springframework.web.bind.annotation.*;

@RestController
public class AuthorizationNController {
    private final AuthorizationNService authorizationNService;

    public AuthorizationNController(AuthorizationNService authorizationNService) {
        this.authorizationNService = authorizationNService;
    }

    @GetMapping("/lab2/authorizations")
    public ResponseEntity<?> getActivityJournals(){
        return ResponseEntity.ok(authorizationNService.getAll());
    }

    @GetMapping("/lab2/authorization/{id}")
    public ResponseEntity<?> findAuthorizationByID(@PathVariable long id){
        return ResponseEntity.ok(authorizationNService.findByID(id));
    }

    @DeleteMapping("/lab2/deleteAuthorization/{id}")
    public void deleteAuthorizationByID(@PathVariable long id){
        authorizationNService.deleteByID(id);
    }

    @RequestMapping(value = "/lab2/updateAuthorization/{id}/{username}", method = RequestMethod.GET)
    public void updateActivityJournalByID(@PathVariable("id") long id, @PathVariable("username") String username){
        authorizationNService.updateByID(id, username);
    }
}
