package kz.aitu.AdvJavaLab2.controller;

import kz.aitu.AdvJavaLab2.service.FondService;
import org.springframework.http.ResponseEntity;
import org.springframework.web.bind.annotation.*;

@RestController
public class FondController {
    private final FondService fondService;

    public FondController(FondService fondService) {
        this.fondService = fondService;
    }

    @GetMapping("/lab2/fonds")
    public ResponseEntity<?> getFonds() {
        return ResponseEntity.ok(fondService.getAll());
    }

    @GetMapping("/lab2/fond/{id}")
    public ResponseEntity<?> findFondByID(@PathVariable long id){
        return ResponseEntity.ok(fondService.findByID(id));
    }

    @DeleteMapping("/lab2/deleteFond/{id}")
    public void deleteFondByID(@PathVariable long id){
        fondService.deleteByID(id);
    }

    @RequestMapping(value = "/lab2/updateFond/{id}/{fondNumber}", method = RequestMethod.GET)
    public void updateFondByID(@PathVariable("id") long id, @PathVariable("fondNumber") String fondNumber){
        fondService.updateByID(id, fondNumber);
    }
}
