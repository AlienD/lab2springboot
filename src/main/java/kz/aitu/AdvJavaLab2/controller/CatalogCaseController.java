package kz.aitu.AdvJavaLab2.controller;

import kz.aitu.AdvJavaLab2.service.CatalogCaseService;
import org.springframework.http.ResponseEntity;
import org.springframework.web.bind.annotation.*;

@RestController
public class CatalogCaseController {
    private final CatalogCaseService catalogCaseService;

    public CatalogCaseController(CatalogCaseService catalogCaseService) {
        this.catalogCaseService = catalogCaseService;
    }

    @GetMapping("/lab2/catalogCases")
    public ResponseEntity<?> getCatalogCases() {
        return ResponseEntity.ok(catalogCaseService.getAll());
    }

    @GetMapping("/lab2/catalogCase/{id}")
    public ResponseEntity<?> findCatalogCaseByID(@PathVariable long id){
        return ResponseEntity.ok(catalogCaseService.findByID(id));
    }

    @DeleteMapping("/lab2/deleteCatalogCase/{id}")
    public void deleteCatalogCaseByID(@PathVariable long id){
        catalogCaseService.deleteByID(id);
    }

    @RequestMapping(value = "/lab2/updateCatalogCase/{id}/{createdBy}", method = RequestMethod.GET)
    public void updateCatalogCaseByID(@PathVariable("id") long id, @PathVariable("createdBy") long createdBy){
        catalogCaseService.updateByID(id, createdBy);
    }
}
