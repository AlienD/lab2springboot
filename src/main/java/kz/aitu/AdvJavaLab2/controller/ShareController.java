package kz.aitu.AdvJavaLab2.controller;

import kz.aitu.AdvJavaLab2.service.ShareService;
import org.springframework.http.ResponseEntity;
import org.springframework.web.bind.annotation.*;

@RestController
public class ShareController {
    private final ShareService shareService;

    public ShareController(ShareService shareService) {
        this.shareService = shareService;
    }

    @GetMapping("/lab2/shares")
    public ResponseEntity<?> getShares() {
        return ResponseEntity.ok(shareService.getAll());
    }

    @GetMapping("/lab2/share/{id}")
    public ResponseEntity<?> findShareByID(@PathVariable long id){
        return ResponseEntity.ok(shareService.findByID(id));
    }

    @DeleteMapping("/lab2/deleteShare/{id}")
    public void deleteShareByID(@PathVariable long id){
        shareService.deleteByID(id);
    }

    @RequestMapping(value = "/lab2/updateShare/{id}/{note}", method = RequestMethod.GET)
    public void updateShareByID(@PathVariable("id") long id, @PathVariable("note") String note){
        shareService.updateByID(id, note);
    }
}
