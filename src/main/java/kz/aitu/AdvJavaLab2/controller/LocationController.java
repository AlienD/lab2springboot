package kz.aitu.AdvJavaLab2.controller;

import kz.aitu.AdvJavaLab2.service.LocationService;
import org.springframework.http.ResponseEntity;
import org.springframework.web.bind.annotation.*;

@RestController
public class LocationController {
    private final LocationService locationService;

    public LocationController(LocationService locationService) {
        this.locationService = locationService;
    }

    @GetMapping("/lab2/locations")
    public ResponseEntity<?> getLocations() {
        return ResponseEntity.ok(locationService.getAll());
    }

    @GetMapping("/lab2/location/{id}")
    public ResponseEntity<?> findLocationByID(@PathVariable long id){
        return ResponseEntity.ok(locationService.findByID(id));
    }

    @DeleteMapping("/lab2/deleteLocation/{id}")
    public void deleteLocationByID(@PathVariable long id){
        locationService.deleteByID(id);
    }

    @RequestMapping(value = "/lab2/updateLocation/{id}/{box}", method = RequestMethod.GET)
    public void updateLocationByID(@PathVariable("id") long id, @PathVariable("box") String box){
        locationService.updateByID(id, box);
    }
}
