package kz.aitu.AdvJavaLab2.controller;

import kz.aitu.AdvJavaLab2.service.CaseEService;
import org.springframework.http.ResponseEntity;
import org.springframework.web.bind.annotation.*;

@RestController
public class CaseEController {
    private final CaseEService caseEService;

    public CaseEController(CaseEService caseEService) {
        this.caseEService = caseEService;
    }

    @GetMapping("/lab2/cases")
    public ResponseEntity<?> getCases() {
        return ResponseEntity.ok(caseEService.getAll());
    }

    @GetMapping("/lab2/case/{id}")
    public ResponseEntity<?> findCaseByID(@PathVariable long id){
        return ResponseEntity.ok(caseEService.findByID(id));
    }

    @DeleteMapping("/lab2/deleteCase/{id}")
    public void deleteCaseByID(@PathVariable long id){
        caseEService.deleteByID(id);
    }

    @RequestMapping(value = "/lab2/updateCase/{id}/{caseHeadingEN}", method = RequestMethod.GET)
    public void updateCaseByID(@PathVariable("id") long id, @PathVariable("caseHeadingEN") String caseHeadingEN){
        caseEService.updateByID(id, caseHeadingEN);
    }
}
