package kz.aitu.AdvJavaLab2.controller;

import kz.aitu.AdvJavaLab2.service.NotificationService;
import org.springframework.http.ResponseEntity;
import org.springframework.web.bind.annotation.*;

@RestController
public class NotificationController {
    private final NotificationService notificationService;

    public NotificationController(NotificationService notificationService) {
        this.notificationService = notificationService;
    }

    @GetMapping("/lab2/notifications")
    public ResponseEntity<?> getNotifications() {
        return ResponseEntity.ok(notificationService.getAll());
    }

    @GetMapping("/lab2/notification/{id}")
    public ResponseEntity<?> findNotificationByID(@PathVariable long id){
        return ResponseEntity.ok(notificationService.findByID(id));
    }

    @DeleteMapping("/lab2/deleteNotification/{id}")
    public void deleteNotificationByID(@PathVariable long id){
        notificationService.deleteByID(id);
    }

    @RequestMapping(value = "/lab2/updateNotification/{id}/{objectType}", method = RequestMethod.GET)
    public void updateNotificationByID(@PathVariable("id") long id, @PathVariable("objectType") String objectType){
        notificationService.updateByID(id, objectType);
    }
}
