package kz.aitu.AdvJavaLab2.controller;

import kz.aitu.AdvJavaLab2.service.TempFilesService;
import org.springframework.http.ResponseEntity;
import org.springframework.web.bind.annotation.*;

@RestController
public class TempFilesController {
    private final TempFilesService tempFilesService;

    public TempFilesController(TempFilesService tempFilesService) {
        this.tempFilesService = tempFilesService;
    }

    @GetMapping("/lab2/tempFiles")
    public ResponseEntity<?> getTempFiles() {
        return ResponseEntity.ok(tempFilesService.getAll());
    }

    @GetMapping("/lab2/tempFiles/{id}")
    public ResponseEntity<?> findTempFilesByID(@PathVariable long id){
        return ResponseEntity.ok(tempFilesService.findByID(id));
    }

    @DeleteMapping("/lab2/deleteTempFiles/{id}")
    public void deleteTempFilesByID(@PathVariable long id){
        tempFilesService.deleteByID(id);
    }

    @RequestMapping(value = "/lab2/updateTempFiles/{id}/{fileBinaryByte}", method = RequestMethod.GET)
    public void updateTempFilesByID(@PathVariable("id") long id, @PathVariable("fileBinaryByte") short fileBinaryByte){
        tempFilesService.updateByID(id, fileBinaryByte);
    }
}
