package kz.aitu.AdvJavaLab2.controller;

import kz.aitu.AdvJavaLab2.service.CompanyUnitService;
import org.springframework.http.ResponseEntity;
import org.springframework.web.bind.annotation.*;

@RestController
public class CompanyUnitController {
    private final CompanyUnitService companyUnitService;

    public CompanyUnitController(CompanyUnitService companyUnitService) {
        this.companyUnitService = companyUnitService;
    }

    @GetMapping("/lab2/companyUnits")
    public ResponseEntity<?> getCompanyUnits() {
        return ResponseEntity.ok(companyUnitService.getAll());
    }

    @GetMapping("/lab2/companyUnit/{id}")
    public ResponseEntity<?> findCompanyUnitByID(@PathVariable long id){
        return ResponseEntity.ok(companyUnitService.findByID(id));
    }

    @DeleteMapping("/lab2/deleteCompanyUnit/{id}")
    public void deleteCompanyUnitByID(@PathVariable long id){
        companyUnitService.deleteByID(id);
    }

    @RequestMapping(value = "/lab2/updateCompanyUnit/{id}/{nameEN}", method = RequestMethod.GET)
    public void updateCompanyUnitByID(@PathVariable("id") long id, @PathVariable("nameEN") String nameEN){
        companyUnitService.updateByID(id, nameEN);
    }

}
