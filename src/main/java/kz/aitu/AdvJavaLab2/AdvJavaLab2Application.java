package kz.aitu.AdvJavaLab2;

import org.springframework.boot.SpringApplication;
import org.springframework.boot.autoconfigure.SpringBootApplication;

@SpringBootApplication
public class AdvJavaLab2Application {

	public static void main(String[] args) {
		SpringApplication.run(AdvJavaLab2Application.class, args);
	}

}
