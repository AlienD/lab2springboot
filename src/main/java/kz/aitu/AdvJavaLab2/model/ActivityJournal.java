package kz.aitu.AdvJavaLab2.model;

import lombok.Data;
import lombok.NoArgsConstructor;

import javax.persistence.Column;
import javax.persistence.Entity;
import javax.persistence.Id;
import javax.persistence.Table;

@Data
@NoArgsConstructor
@Entity
@Table(name = "activityjournal")
public class ActivityJournal {
    @Id
    private long id;
    private String eventtype;
    private String objecttype;
    private long objectid;
    private long createdtimestamp;
    private long createdby;
    private String messagelevel;
    private String message;
}
