package kz.aitu.AdvJavaLab2.model;

import lombok.Data;
import lombok.NoArgsConstructor;

import javax.persistence.Entity;
import javax.persistence.Id;
import javax.persistence.Table;

@Data
@NoArgsConstructor
@Entity
@Table(name = "companyunit")
public class CompanyUnit {
    @Id
    private long id;
    private String nameru;
    private String namekz;
    private String nameen;
    private long parentid;
    private int year;
    private int companyid;
    private String codeindex;
    private long createdtimestamp;
    private long createdby;
    private long updatedtimestamp;
    private long updatedby;
}
