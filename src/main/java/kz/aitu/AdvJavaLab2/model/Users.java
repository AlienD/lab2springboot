package kz.aitu.AdvJavaLab2.model;

import lombok.Data;
import lombok.NoArgsConstructor;

import javax.persistence.Entity;
import javax.persistence.Id;
import javax.persistence.Table;

@Data
@NoArgsConstructor
@Entity
@Table(name = "users")
public class Users {
    @Id
    private long id;
    private long authid;
    private String name;
    private String fullname;
    private String surname;
    private String secondname;
    private String status;
    private long companyunitid;
    private String password;
    private long lastlogintimestamp;
    private String iin;
    private boolean isactive;
    private boolean isactivated;
    private long createdtimestamp;
    private long createdby;
    private long updatedtimestamp;
    private long updatedby;
}
