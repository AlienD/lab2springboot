package kz.aitu.AdvJavaLab2.model;

import lombok.Data;
import lombok.NoArgsConstructor;

import javax.persistence.Entity;
import javax.persistence.Id;
import javax.persistence.Table;

@Data
@NoArgsConstructor
@Entity
@Table(name = "historyrequeststatus")
public class HistoryRequestStatus {
    @Id
    private long id;
    private long requestid;
    private String status;
    private long createdtimestamp;
    private long createdby;
    private long updatedtimestamp;
    private long updatedby;
}
