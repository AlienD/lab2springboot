package kz.aitu.AdvJavaLab2.model;

import lombok.Data;
import lombok.NoArgsConstructor;

import javax.persistence.Entity;
import javax.persistence.Id;
import javax.persistence.Table;

@Data
@NoArgsConstructor
@Entity
@Table(name = "company")
public class Company {
    @Id
    private long id;
    private String nameru;
    private String namekz;
    private String nameen;
    private String bin;
    private long parentid;
    private long fondid;
    private long createdtimestamp;
    private long createdby;
    private long updatedtimestamp;
    private long updatedby;
}
