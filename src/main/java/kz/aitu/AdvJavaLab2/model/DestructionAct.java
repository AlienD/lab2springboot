package kz.aitu.AdvJavaLab2.model;


import lombok.Data;
import lombok.NoArgsConstructor;

import javax.persistence.Entity;
import javax.persistence.Id;
import javax.persistence.Table;

@Data
@NoArgsConstructor
@Entity
@Table(name = "destructionact")
public class DestructionAct {
    @Id
    private long id;
    private String actnumber;
    private String base;
    private long structuralsubdivisionid;
    private long createdtimestamp;
    private long createdby;
    private long updatedtimestamp;
    private long updatedby;
}
