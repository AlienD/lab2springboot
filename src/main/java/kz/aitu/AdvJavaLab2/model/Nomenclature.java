package kz.aitu.AdvJavaLab2.model;

import lombok.Data;
import lombok.NoArgsConstructor;

import javax.persistence.Entity;
import javax.persistence.Id;
import javax.persistence.Table;

@Data
@NoArgsConstructor
@Entity
@Table(name = "nomenclature")
public class Nomenclature {
    @Id
    private long id;
    private String nomenclaturenumber;
    private int year;
    private long nomenclaturesummaryid;
    private long companyunitid;
    private long createdtimestamp;
    private long createdby;
    private long updatedtimestamp;
    private long updatedby;
}
