package kz.aitu.AdvJavaLab2.model;

import lombok.Data;
import lombok.NoArgsConstructor;

import javax.persistence.Entity;
import javax.persistence.Id;
import javax.persistence.Table;

@Data
@NoArgsConstructor
@Entity
@Table(name = "casee")
public class CaseE {
    @Id
    private long id;
    private String casenumber;
    private String casetom;
    private String caseheadingru;
    private String caseheadingkz;
    private String caseheadingen;
    private long startdate;
    private long finishdate;
    private long pagenumber;
    private boolean Eds;
    private String edssignature;
    private boolean sendingnaf;
    private boolean deletionsign;
    private boolean limitedaccess;
    private String hash;
    private int version;
    private String idversion;
    private boolean activeversion;
    private String note;
    private long locationid;
    private long caseindexid;
    private long inventoryid;
    private long destructionactid;
    private long structuralsubdivisionid;
    private String caseblockchainaddress;
    private long addblockchaindate;
    private long createdtimestamp;
    private long createdby;
    private long updatedtimestamp;
    private long updatedby;
}
