package kz.aitu.AdvJavaLab2.model;

import lombok.Data;
import lombok.NoArgsConstructor;

import javax.persistence.Entity;
import javax.persistence.Id;
import javax.persistence.Table;

@Data
@NoArgsConstructor
@Entity
@Table(name = "record")
public class Record {
    @Id
    private long id;
    private String number;
    private String type;
    private long companyunitid;
    private long createdtimestamp;
    private long createdby;
    private long updatedtimestamp;
    private long updatedby;
}
