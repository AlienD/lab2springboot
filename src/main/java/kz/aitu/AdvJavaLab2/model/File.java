package kz.aitu.AdvJavaLab2.model;

import lombok.Data;
import lombok.NoArgsConstructor;

import javax.persistence.Entity;
import javax.persistence.Id;
import javax.persistence.Table;

@Data
@NoArgsConstructor
@Entity
@Table(name = "file")
public class File {
    @Id
    private long id;
    private String name;
    private String type;
    private long size;
    private int pagecount;
    private String hash;
    private boolean isdeleted;
    private long filebinaryid;
    private long createdtimestamp;
    private long createdby;
    private long updatedtimestamp;
    private long updatedby;
}
