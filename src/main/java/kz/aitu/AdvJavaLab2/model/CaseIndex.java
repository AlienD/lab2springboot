package kz.aitu.AdvJavaLab2.model;

import lombok.Data;
import lombok.NoArgsConstructor;

import javax.persistence.Entity;
import javax.persistence.Id;
import javax.persistence.Table;

@Data
@NoArgsConstructor
@Entity
@Table(name = "caseindex")
public class CaseIndex {
    @Id
    private long id;
    private String caseindex;
    private String titleru;
    private String titlekz;
    private String titleen;
    private int storagetype;
    private int storageyear;
    private String note;
    private long companyunitid;
    private long nomenclatureid;
    private long createdtimestamp;
    private long createdby;
    private long updatedtimestamp;
    private long updatedby;
}
