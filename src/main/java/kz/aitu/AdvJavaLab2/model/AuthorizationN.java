package kz.aitu.AdvJavaLab2.model;

import lombok.Data;
import lombok.NoArgsConstructor;

import javax.persistence.Column;
import javax.persistence.Entity;
import javax.persistence.Id;
import javax.persistence.Table;

@Data
@NoArgsConstructor
@Entity
@Table(name = "authorizationn")
public class AuthorizationN {
    @Id
    private long id;
    private String username;
    private String email;
    private String password;
    private String role;
    private String forgotpasswordkey;
    private long forgotpasswordkeytimestamp;
    private long companyunitid;
}
