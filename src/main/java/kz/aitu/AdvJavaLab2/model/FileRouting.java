package kz.aitu.AdvJavaLab2.model;

import lombok.Data;
import lombok.NoArgsConstructor;

import javax.persistence.Entity;
import javax.persistence.Id;
import javax.persistence.Table;

@Data
@NoArgsConstructor
@Entity
@Table(name = "filerouting")
public class FileRouting {
    @Id
    private long id;
    private long fileid;
    private String tablename;
    private long tableid;
    private String type;
}
