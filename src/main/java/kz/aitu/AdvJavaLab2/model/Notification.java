package kz.aitu.AdvJavaLab2.model;

import lombok.Data;
import lombok.NoArgsConstructor;

import javax.persistence.Entity;
import javax.persistence.Id;
import javax.persistence.Table;

@Data
@NoArgsConstructor
@Entity
@Table(name = "notification")
public class Notification {
    @Id
    private long id;
    private String objecttype;
    private long objectid;
    private long companyunitid;
    private long userid;
    private long createdtimestamp;
    private long viewedtimestamp;
    private boolean isviewed;
    private String title;
    private String text;
    private long companyid;
}
