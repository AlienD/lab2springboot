package kz.aitu.AdvJavaLab2.model;

import lombok.Data;
import lombok.NoArgsConstructor;

import javax.persistence.Entity;
import javax.persistence.Id;
import javax.persistence.Table;

@Data
@NoArgsConstructor
@Entity
@Table(name = "share")
public class Share {
    @Id
    private long id;
    private long requestid;
    private String note;
    private long senderid;
    private long receiverid;
    private long sharetimestamp;
}
