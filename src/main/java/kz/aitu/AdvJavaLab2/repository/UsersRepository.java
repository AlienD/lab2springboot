package kz.aitu.AdvJavaLab2.repository;

import kz.aitu.AdvJavaLab2.model.AuthorizationN;
import kz.aitu.AdvJavaLab2.model.Users;
import org.springframework.data.jpa.repository.Modifying;
import org.springframework.data.jpa.repository.Query;
import org.springframework.data.repository.CrudRepository;
import org.springframework.data.repository.query.Param;
import org.springframework.stereotype.Repository;

import javax.transaction.Transactional;

@Repository
public interface UsersRepository extends CrudRepository<Users, Long> {
    @Modifying
    @Transactional
    @Query("update Users c set c.name =:name where c.id =:ID")
    public void updateNameByID(@Param("name") String name, @Param("ID") long id);
}
