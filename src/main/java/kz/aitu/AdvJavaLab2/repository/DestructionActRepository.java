package kz.aitu.AdvJavaLab2.repository;

import kz.aitu.AdvJavaLab2.model.AuthorizationN;
import kz.aitu.AdvJavaLab2.model.DestructionAct;
import org.springframework.data.jpa.repository.Modifying;
import org.springframework.data.jpa.repository.Query;
import org.springframework.data.repository.CrudRepository;
import org.springframework.data.repository.query.Param;
import org.springframework.stereotype.Repository;

import javax.transaction.Transactional;

@Repository
public interface DestructionActRepository extends CrudRepository<DestructionAct, Long> {
    @Modifying
    @Transactional
    @Query("update DestructionAct c set c.base =:base where c.id =:ID")
    public void updateBaseByID(@Param("base") String base, @Param("ID") long id);
}
