package kz.aitu.AdvJavaLab2.repository;

import kz.aitu.AdvJavaLab2.model.AuthorizationN;
import kz.aitu.AdvJavaLab2.model.CompanyUnit;
import org.springframework.data.jpa.repository.Modifying;
import org.springframework.data.jpa.repository.Query;
import org.springframework.data.repository.CrudRepository;
import org.springframework.data.repository.query.Param;
import org.springframework.stereotype.Repository;

import javax.transaction.Transactional;

@Repository
public interface CompanyUnitRepository extends CrudRepository<CompanyUnit, Long> {
    @Modifying
    @Transactional
    @Query("update CompanyUnit c set c.nameen =:nameEN where c.id =:ID")
    public void updateNameEnByID(@Param("nameEN") String nameEN, @Param("ID") long id);
}
