package kz.aitu.AdvJavaLab2.repository;

import kz.aitu.AdvJavaLab2.model.AuthorizationN;
import kz.aitu.AdvJavaLab2.model.Share;
import org.springframework.data.jpa.repository.Modifying;
import org.springframework.data.jpa.repository.Query;
import org.springframework.data.repository.CrudRepository;
import org.springframework.data.repository.query.Param;
import org.springframework.stereotype.Repository;

import javax.transaction.Transactional;

@Repository
public interface ShareRepository extends CrudRepository<Share, Long> {
    @Modifying
    @Transactional
    @Query("update Share c set c.note =:note where c.id =:ID")
    public void updateNoteByID(@Param("note") String note, @Param("ID") long id);
}
