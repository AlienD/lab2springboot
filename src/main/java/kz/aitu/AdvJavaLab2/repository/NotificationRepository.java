package kz.aitu.AdvJavaLab2.repository;

import kz.aitu.AdvJavaLab2.model.AuthorizationN;
import kz.aitu.AdvJavaLab2.model.Notification;
import org.springframework.data.jpa.repository.Modifying;
import org.springframework.data.jpa.repository.Query;
import org.springframework.data.repository.CrudRepository;
import org.springframework.data.repository.query.Param;
import org.springframework.stereotype.Repository;

import javax.transaction.Transactional;

@Repository
public interface NotificationRepository extends CrudRepository<Notification, Long> {
    @Modifying
    @Transactional
    @Query("update Notification c set c.objecttype =:objectType where c.id =:ID")
    public void updateObjectTypeByID(@Param("objectType") String objectType, @Param("ID") long id);
}
