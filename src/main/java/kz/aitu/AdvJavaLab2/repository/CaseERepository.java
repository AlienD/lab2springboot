package kz.aitu.AdvJavaLab2.repository;

import kz.aitu.AdvJavaLab2.model.AuthorizationN;
import kz.aitu.AdvJavaLab2.model.CaseE;
import org.springframework.data.jpa.repository.Modifying;
import org.springframework.data.jpa.repository.Query;
import org.springframework.data.repository.CrudRepository;
import org.springframework.data.repository.query.Param;
import org.springframework.stereotype.Repository;

import javax.transaction.Transactional;

@Repository
public interface CaseERepository extends CrudRepository<CaseE, Long>  {
    @Modifying
    @Transactional
    @Query("update CaseE c set c.caseheadingen =:caseHeadingEN where c.id =:ID")
    public void updateCaseHeadingEnByID(@Param("caseHeadingEN") String caseHeadingEN, @Param("ID") long id);
}
