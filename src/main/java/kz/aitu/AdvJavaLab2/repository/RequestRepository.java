package kz.aitu.AdvJavaLab2.repository;

import kz.aitu.AdvJavaLab2.model.AuthorizationN;
import kz.aitu.AdvJavaLab2.model.Request;
import org.springframework.data.jpa.repository.Modifying;
import org.springframework.data.jpa.repository.Query;
import org.springframework.data.repository.CrudRepository;
import org.springframework.data.repository.query.Param;
import org.springframework.stereotype.Repository;

import javax.transaction.Transactional;

@Repository
public interface RequestRepository extends CrudRepository<Request, Long> {
    @Modifying
    @Transactional
    @Query("update Request c set c.requestuserid =:requestUserId where c.id =:ID")
    public void updateRequestUserIdByID(@Param("requestUserId") long requestUserId, @Param("ID") long id);
}
