package kz.aitu.AdvJavaLab2.repository;

import kz.aitu.AdvJavaLab2.model.AuthorizationN;
import kz.aitu.AdvJavaLab2.model.Location;
import org.springframework.data.jpa.repository.Modifying;
import org.springframework.data.jpa.repository.Query;
import org.springframework.data.repository.CrudRepository;
import org.springframework.data.repository.query.Param;
import org.springframework.stereotype.Repository;

import javax.transaction.Transactional;

@Repository
public interface LocationRepository extends CrudRepository<Location, Long> {
    @Modifying
    @Transactional
    @Query("update Location c set c.box =:box where c.id =:ID")
    public void updateBoxByID(@Param("box") String box, @Param("ID") long id);
}
