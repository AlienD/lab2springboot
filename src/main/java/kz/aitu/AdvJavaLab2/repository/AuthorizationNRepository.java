package kz.aitu.AdvJavaLab2.repository;

import kz.aitu.AdvJavaLab2.model.ActivityJournal;
import kz.aitu.AdvJavaLab2.model.AuthorizationN;
import org.springframework.data.jpa.repository.Modifying;
import org.springframework.data.jpa.repository.Query;
import org.springframework.data.repository.CrudRepository;
import org.springframework.data.repository.query.Param;
import org.springframework.stereotype.Repository;

import javax.transaction.Transactional;

@Repository
public interface AuthorizationNRepository extends CrudRepository<AuthorizationN, Long> {
    @Modifying
    @Transactional
    @Query("update AuthorizationN c set c.username =:username where c.id =:ID")
    public void updateUsernameByID(@Param("username") String username, @Param("ID") long id);
}
