package kz.aitu.AdvJavaLab2.repository;

import jdk.jfr.EventType;
import kz.aitu.AdvJavaLab2.model.ActivityJournal;
import org.springframework.data.jpa.repository.Modifying;
import org.springframework.data.jpa.repository.Query;
import org.springframework.data.repository.CrudRepository;
import org.springframework.data.repository.query.Param;
import org.springframework.stereotype.Repository;

import javax.transaction.Transactional;
import java.util.List;

@Repository
public interface ActivityJournalRepository extends CrudRepository<ActivityJournal, Long> {
    @Modifying
    @Transactional
    @Query("update ActivityJournal c set c.eventtype =:eventType where c.id =:ID")
    public void updateEventTypeByID(@Param("eventType") String EventType, @Param("ID") long id);
}
