package kz.aitu.AdvJavaLab2.repository;

import kz.aitu.AdvJavaLab2.model.AuthorizationN;
import kz.aitu.AdvJavaLab2.model.Nomenclature;
import org.springframework.data.jpa.repository.Modifying;
import org.springframework.data.jpa.repository.Query;
import org.springframework.data.repository.CrudRepository;
import org.springframework.data.repository.query.Param;
import org.springframework.stereotype.Repository;

import javax.transaction.Transactional;

@Repository
public interface NomenclatureRepository extends CrudRepository<Nomenclature, Long> {
    @Modifying
    @Transactional
    @Query("update Nomenclature c set c.year =:year where c.id =:ID")
    public void updateYearByID(@Param("year") int year, @Param("ID") long id);
}
