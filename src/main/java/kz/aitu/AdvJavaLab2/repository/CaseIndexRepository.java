package kz.aitu.AdvJavaLab2.repository;

import kz.aitu.AdvJavaLab2.model.AuthorizationN;
import kz.aitu.AdvJavaLab2.model.CaseIndex;
import org.springframework.data.jpa.repository.Modifying;
import org.springframework.data.jpa.repository.Query;
import org.springframework.data.repository.CrudRepository;
import org.springframework.data.repository.query.Param;
import org.springframework.stereotype.Repository;

import javax.transaction.Transactional;

@Repository
public interface CaseIndexRepository extends CrudRepository<CaseIndex, Long> {
    @Modifying
    @Transactional
    @Query("update CaseIndex c set c.titleen =:titleEN where c.id =:ID")
    public void updateTitleEnByID(@Param("titleEN") String titleEN, @Param("ID") long id);
}
