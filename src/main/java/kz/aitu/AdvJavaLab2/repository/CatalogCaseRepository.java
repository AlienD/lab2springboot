package kz.aitu.AdvJavaLab2.repository;

import kz.aitu.AdvJavaLab2.model.AuthorizationN;
import kz.aitu.AdvJavaLab2.model.CatalogCase;
import org.springframework.data.jpa.repository.Modifying;
import org.springframework.data.jpa.repository.Query;
import org.springframework.data.repository.CrudRepository;
import org.springframework.data.repository.query.Param;
import org.springframework.stereotype.Repository;

import javax.transaction.Transactional;

@Repository
public interface CatalogCaseRepository extends CrudRepository<CatalogCase, Long> {
    @Modifying
    @Transactional
    @Query("update CatalogCase c set c.createdby =:createdBy where c.id =:ID")
    public void updateCreatedByByID(@Param("createdBy") long createdBy, @Param("ID") long id);
}
