package kz.aitu.AdvJavaLab2.repository;

import kz.aitu.AdvJavaLab2.model.AuthorizationN;
import kz.aitu.AdvJavaLab2.model.Record;
import org.springframework.data.jpa.repository.Modifying;
import org.springframework.data.jpa.repository.Query;
import org.springframework.data.repository.CrudRepository;
import org.springframework.data.repository.query.Param;
import org.springframework.stereotype.Repository;

import javax.transaction.Transactional;

@Repository
public interface RecordRepository extends CrudRepository<Record, Long> {
    @Modifying
    @Transactional
    @Query("update Record c set c.number =:number where c.id =:ID")
    public void updateNumberByID(@Param("number") String number, @Param("ID") long id);
}
