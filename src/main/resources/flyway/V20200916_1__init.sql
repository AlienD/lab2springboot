drop table if exists ActivityJournal CASCADE;
CREATE TABLE ActivityJournal (
	id BIGSERIAL NOT NULL,
	eventType VARCHAR(128),
	objectType VARCHAR(255),
	objectId BIGINT,
	createdTimestamp BIGINT,
	createdBy BIGINT,
	messageLevel VARCHAR(128),
	message VARCHAR(255),
	PRIMARY KEY (id)
);

drop table if exists AuthorizationN CASCADE;
CREATE TABLE AuthorizationN (
	id BIGSERIAL NOT NULL,
	username VARCHAR(255),
	email VARCHAR(255),
	password VARCHAR(128),
	role VARCHAR(255),
	forgotPasswordKey VARCHAR(128),
	forgotPasswordKeyTimestamp BIGINT,
	companyUnitId BIGINT,
	PRIMARY KEY (id)
);

drop table if exists CaseE CASCADE;
CREATE TABLE CaseE (
	id BIGSERIAL NOT NULL,
	caseNumber VARCHAR(128),
	caseTom VARCHAR(128),
	caseHeadingRu VARCHAR(128),
	caseHeadingKz VARCHAR(128),
	caseHeadingEn VARCHAR(128),
	startDate BIGINT,
	finishDate BIGINT,
	pageNumber BIGINT,
	Eds BOOLEAN,
	edsSignature TEXT,
	sendingNaf BOOLEAN,
	deletionSign BOOLEAN,
	limitedAccess BOOLEAN,
	hash VARCHAR(128),
	version INT,
	idVersion VARCHAR(128),
	activeVersion BOOLEAN,
	note VARCHAR(255),
	locationId BIGINT,
	caseIndexId BIGINT,
	inventoryId BIGINT,
	destructionActId BIGINT,
	structuralSubdivisionId BIGINT,
	caseBlockchainAddress VARCHAR(128),
	addBlockchainDate BIGINT,
	createdTimestamp BIGINT,
	createdBy BIGINT,
	updatedTimestamp BIGINT,
	updatedBy BIGINT,
	PRIMARY KEY (id)
);

drop table if exists CaseIndex CASCADE;
CREATE TABLE CaseIndex (
	id BIGSERIAL NOT NULL,
	caseIndex VARCHAR(128),
    titleRu VARCHAR(128),
	titleKz VARCHAR(128),
	titleEn VARCHAR(128),
	storageType INT,
	storageYear INT,
	note VARCHAR(128),
	companyUnitId BIGINT,
	nomenclatureId BIGINT,
	createdTimestamp BIGINT,
	createdBy BIGINT,
	updatedTimestamp BIGINT,
	updatedBy BIGINT,
	PRIMARY KEY (id)
);

drop table if exists Catalog CASCADE;
CREATE TABLE Catalog (
	id BIGSERIAL NOT NULL,
	nameRu VARCHAR(128),
    nameKz VARCHAR(128),
	nameEn VARCHAR(128),
	parentId BIGINT,
	companyUnitId BIGINT,
	createdTimestamp BIGINT,
	createdBy BIGINT,
	updatedTimestamp BIGINT,
	updatedBy BIGINT,
	PRIMARY KEY (id)
);

drop table if exists CatalogCase CASCADE;
CREATE TABLE CatalogCase (
	id BIGSERIAL NOT NULL,
    caseId BIGINT,
	catalogId BIGINT,
	companyUnitId BIGINT,
	createdTimestamp BIGINT,
	createdBy BIGINT,
	updatedTimestamp BIGINT,
	updatedBy BIGINT,
	PRIMARY KEY (id)
);

drop table if exists Company CASCADE;
CREATE TABLE Company (
	id BIGSERIAL NOT NULL,
	nameRu VARCHAR(128),
    nameKz VARCHAR(128),
	nameEn VARCHAR(128),
	bin VARCHAR(32),
    parentId BIGINT,
	fondId BIGINT,
	createdTimestamp BIGINT,
	createdBy BIGINT,
	updatedTimestamp BIGINT,
	updatedBy BIGINT,
	PRIMARY KEY (id)
);

drop table if exists CompanyUnit CASCADE;
CREATE TABLE CompanyUnit (
	id BIGSERIAL NOT NULL,
	nameRu VARCHAR(128),
    nameKz VARCHAR(128),
	nameEn VARCHAR(128),
	parentId BIGINT,
	year INT,
    companyId INT,
	codeIndex VARCHAR(16),
	createdTimestamp BIGINT,
	createdBy BIGINT,
	updatedTimestamp BIGINT,
	updatedBy BIGINT,
	PRIMARY KEY (id)
);

drop table if exists DestructionAct CASCADE;
CREATE TABLE DestructionAct (
	id BIGSERIAL NOT NULL,
	actNumber VARCHAR(128),
    base VARCHAR(128),
	structuralSubdivisionId BIGINT,
	createdTimestamp BIGINT,
	createdBy BIGINT,
	updatedTimestamp BIGINT,
	updatedBy BIGINT,
	PRIMARY KEY (id)
);

drop table if exists File CASCADE;
CREATE TABLE File (
	id BIGSERIAL NOT NULL,
	name VARCHAR(128),
    type VARCHAR(128),
    size BIGINT,
    pageCount INT,
    hash VARCHAR(128),
    isDeleted BOOLEAN,
	fileBinaryId BIGINT,
	createdTimestamp BIGINT,
	createdBy BIGINT,
	updatedTimestamp BIGINT,
	updatedBy BIGINT,
	PRIMARY KEY (id)
);

drop table if exists FileRouting CASCADE;
CREATE TABLE FileRouting (
	id BIGSERIAL NOT NULL,
	fileId BIGINT,
    tableName VARCHAR(128),
    tableId BIGINT,
    type VARCHAR(128),
	PRIMARY KEY (id)
);

drop table if exists Fond CASCADE;
CREATE TABLE Fond (
	id BIGSERIAL NOT NULL,
    fondNumber VARCHAR(128),
    createdTimestamp BIGINT,
	createdBy BIGINT,
	updatedTimestamp BIGINT,
	updatedBy BIGINT,
	PRIMARY KEY (id)
);

drop table if exists HistoryRequestStatus CASCADE;
CREATE TABLE HistoryRequestStatus (
	id BIGSERIAL NOT NULL,
	requestId BIGINT,
    status VARCHAR(64),
    createdTimestamp BIGINT,
	createdBy BIGINT,
	updatedTimestamp BIGINT,
	updatedBy BIGINT,
	PRIMARY KEY (id)
);

drop table if exists Location CASCADE;
CREATE TABLE Location (
	id BIGSERIAL NOT NULL,
    row VARCHAR(64),
    line VARCHAR(64),
    columnN VARCHAR(64),
    box VARCHAR(64),
    companyUnitId BIGINT,
    createdTimestamp BIGINT,
	createdBy BIGINT,
	updatedTimestamp BIGINT,
	updatedBy BIGINT,
	PRIMARY KEY (id)
);


drop table if exists Nomenclature CASCADE;
CREATE TABLE Nomenclature (
	id BIGSERIAL NOT NULL,
    nomenclatureNumber VARCHAR(128),
    year INT,
    nomenclatureSummaryId BIGINT,
    companyUnitId BIGINT,
    createdTimestamp BIGINT,
	createdBy BIGINT,
	updatedTimestamp BIGINT,
	updatedBy BIGINT,
	PRIMARY KEY (id)
);

drop table if exists NomenclatureSummary CASCADE;
CREATE TABLE NomenclatureSummary(
	id BIGSERIAL NOT NULL,
    number VARCHAR(128),
    year INT,
    companyUnitId BIGINT,
    createdTimestamp BIGINT,
	createdBy BIGINT,
	updatedTimestamp BIGINT,
	updatedBy BIGINT,
	PRIMARY KEY (id)
);

drop table if exists Notification CASCADE;
CREATE TABLE Notification (
	id BIGSERIAL NOT NULL,
    objectType VARCHAR(128),
    objectId BIGINT,
    companyUnitId BIGINT,
    userId BIGINT,
    createdTimestamp BIGINT,
	viewedTimestamp BIGINT,
	isViewed BOOLEAN,
	title VARCHAR(255),
	text VARCHAR(255),
	companyId BIGINT,
	PRIMARY KEY (id)
);

drop table if exists Record CASCADE;
CREATE TABLE Record (
	id BIGSERIAL NOT NULL,
    number VARCHAR(128),
    type VARCHAR(128),
    companyUnitId BIGINT,
    createdTimestamp BIGINT,
	createdBy BIGINT,
	updatedTimestamp BIGINT,
	updatedBy BIGINT,
	PRIMARY KEY (id)
);

drop table if exists Request CASCADE ;
CREATE TABLE Request (
	id BIGSERIAL NOT NULL,
    requestUserId BIGINT,
    responseUserId BIGINT,
    caseId BIGINT,
    caseIndexId BIGINT,
    createdType VARCHAR(64),
    comment VARCHAR(255),
    status VARCHAR(64),
	timestamp BIGINT,
	shareStart BIGINT,
	shareFinish BIGINT,
	favorite BOOLEAN,
	updateTimestamp BIGINT,
	updateBy BIGINT,
	declineNote VARCHAR(255),
	companyUnitId BIGINT,
	fromRequestId BIGINT,
	PRIMARY KEY (id)
);

drop table if exists SearchKey CASCADE;
CREATE TABLE SearchKey (
	id BIGSERIAL NOT NULL,
    name VARCHAR(128),
    companyUnitId BIGINT,
    createdTimestamp BIGINT,
	createdBy BIGINT,
	updatedTimestamp BIGINT,
	updatedBy BIGINT,
	PRIMARY KEY (id)
);

drop table if exists SearchKeyRouting CASCADE;
CREATE TABLE SearchKeyRouting (
	id BIGSERIAL NOT NULL,
    searchKeyId BIGINT,
    tableName VARCHAR(128),
    tableId BIGINT,
	type VARCHAR(128),
	PRIMARY KEY (id)
);

drop table if exists Share CASCADE;
CREATE TABLE Share (
	id BIGSERIAL NOT NULL,
    requestId BIGINT,
    note VARCHAR(255),
    senderId BIGINT,
    receiverId BIGINT,
    shareTimestamp BIGINT,
	PRIMARY KEY (id)
);

drop table if exists TempFiles CASCADE;
CREATE TABLE TempFiles (
	id BIGSERIAL NOT NULL,
    fileBinary TEXT,
    fileBinaryByte SMALLINT,
	PRIMARY KEY (id)
);

drop table if exists Users CASCADE;
CREATE TABLE Users (
	id BIGSERIAL NOT NULL,
    authId BIGINT,
    name VARCHAR(128),
    fullName VARCHAR(128),
    surname VARCHAR(128),
    secondName VARCHAR(128),
    status VARCHAR(128),
    companyUnitId BIGINT,
    password VARCHAR(128),
    lastLoginTimestamp BIGINT,
    iin VARCHAR(32),
    isActive BOOLEAN,
    isActivated BOOLEAN,
    createdTimestamp BIGINT,
	createdBy BIGINT,
	updatedTimestamp BIGINT,
	updatedBy BIGINT,
	PRIMARY KEY (id)
);

