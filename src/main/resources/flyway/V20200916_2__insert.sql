insert into AuthorizationN (username, email, password, role, forgotPasswordKey, forgotPasswordKeyTimestamp, companyUnitId) values ('sramiro0', 'wleyfield0@nsw.gov.au', 'xXlJvYk', 'iShares MSCI Europe Financials Sector Index Fund', 'gnaITCE', 0691362882, 1);
insert into AuthorizationN (username, email, password, role, forgotPasswordKey, forgotPasswordKeyTimestamp, companyUnitId) values ('eschlag1', 'wpilbeam1@wired.com', 'pUj02Q7', 'Novus Therapeutics, Inc.', '4YPlpKhx', 1869991125, 2);
insert into AuthorizationN (username, email, password, role, forgotPasswordKey, forgotPasswordKeyTimestamp, companyUnitId) values ('ndibiasi2', 'ecorragan2@tripadvisor.com', 'skib5P', 'Ocean Bio-Chem, Inc.', 'lGL79O', 1410047946, 2);
insert into AuthorizationN (username, email, password, role, forgotPasswordKey, forgotPasswordKeyTimestamp, companyUnitId) values ('rsommerlin3', 'tperelli3@unicef.org', 'OvWbA7Iq', 'Nuveen California Quality Municipal Income Fund', '3DWemg', 2865643123, 3);
insert into AuthorizationN (username, email, password, role, forgotPasswordKey, forgotPasswordKeyTimestamp, companyUnitId) values ('mnijs4', 'cpharoah4@scientificamerican.com', 'ByuIMV', 'Aldeyra Therapeutics, Inc.', 'nlAd0g', 4079709625, 5);

insert into Fond (fondnumber, createdtimestamp, createdby, updatedtimestamp, updatedby) values ('30838', 5212541913, 354, 1682438996, 4817);
insert into Fond (fondNumber, createdTimestamp, createdBy, updatedTimestamp, updatedBy) values ('625', 2916237267, 4237, 7007052231, 87);
insert into Fond (fondNumber, createdTimestamp, createdBy, updatedTimestamp, updatedBy) values ('4', 2005023906, 6, 2619576563, 4);
insert into Fond (fondNumber, createdTimestamp, createdBy, updatedTimestamp, updatedBy) values ('70971', 8682780550, 5, 5917935742, 164);
insert into Fond (fondNumber, createdTimestamp, createdBy, updatedTimestamp, updatedBy) values ('76', 0065687655, 5, 8179256499, 24);

insert into Company (nameRu, nameKz, nameEn, bin, parentId, fondId, createdTimestamp, createdBy, updatedTimestamp, updatedBy) values ('Муксо', 'Муксо', 'Muxo', '5341852265', 75, 1, 8247017652, 78, 0711429197, 8);
insert into Company (nameRu, nameKz, nameEn, bin, parentId, fondId, createdTimestamp, createdBy, updatedTimestamp, updatedBy) values ('Кокюибокс', 'Кокюибокс', 'Cogibox', '1077009933', 754, 2, 9315319443, 06347, 7203934497, 00109);
insert into Company (nameRu, nameKz, nameEn, bin, parentId, fondId, createdTimestamp, createdBy, updatedTimestamp, updatedBy) values ('Куаксо', 'Куаксо', 'Quaxo', '6227087459', 4803, 4, 3121864165, 64987, 7322436296, 07);
insert into Company (nameRu, nameKz, nameEn, bin, parentId, fondId, createdTimestamp, createdBy, updatedTimestamp, updatedBy) values ('Чаттерпоинт', 'Чаттерпоинт', 'Chatterpoint', '6896568232', 59334, 2, 5816502522, 9, 0077316495, 40008);
insert into Company (nameRu, nameKz, nameEn, bin, parentId, fondId, createdTimestamp, createdBy, updatedTimestamp, updatedBy) values ('ИнноЗи', 'ИнноЗи', 'InnoZ', '3832349960', 765, 3, 6070108108, 821, 7057417748, 29);

insert into CompanyUnit (nameRu, nameKz, nameEn, parentId, year, companyId, codeIndex , createdTimestamp, createdBy, updatedTimestamp, updatedBy) values ('Realblab', 'Ailane', 'Zava', 01083, 2001, 2, '4610623439', 0528664557, 87, 0789490412, 88443);
insert into CompanyUnit (nameRu, nameKz, nameEn, parentId, year, companyId, codeIndex , createdTimestamp, createdBy, updatedTimestamp, updatedBy) values ('Quinu', 'Skipfire', 'Zoombeat', 5172, 2012, 2, '7232931581', 143683661, 69748, 1005873135, 01);
insert into CompanyUnit (nameRu, nameKz, nameEn, parentId, year, companyId, codeIndex , createdTimestamp, createdBy, updatedTimestamp, updatedBy) values ('Gabspot', 'Yotz', 'Zoomcast', 4, 2006, 3, '2254220748', 5884593546, 16, 6832630425, 62);
insert into CompanyUnit (nameRu, nameKz, nameEn, parentId, year, companyId, codeIndex , createdTimestamp, createdBy, updatedTimestamp, updatedBy) values ('Livepath', 'BlogXS', 'Topicstorm', 5551, 2009, 4, '0012056626', 6345355313, 15193, 1637720262, 2974);
insert into CompanyUnit (nameRu, nameKz, nameEn, parentId, year, companyId, codeIndex , createdTimestamp, createdBy, updatedTimestamp, updatedBy) values ('Skipstorm', 'Dabfeed', 'Aivee', 81859, 1996, 3, '1178067920', 9337640910, 3782, 4822069176, 4728);

insert into Users (authId, name, fullName, surname, secondName, status, companyUnitId, password, lastLoginTimestamp, iin, isActive, isActivated, createdTimestamp, createdBy, updatedTimestamp, updatedBy) values (1, 'Roch', 'Roch Kleen', 'Kleen', 'rkleen0', 'Chief Design Engineer', 1, 'WoDXBSv4', 6048272073, '0463159381', true, false, 6894969345, 3, 2301688666, 678);
insert into Users (authId, name, fullName, surname, secondName, status, companyUnitId, password, lastLoginTimestamp, iin, isActive, isActivated, createdTimestamp, createdBy, updatedTimestamp, updatedBy) values (3, 'Sukey', 'Sukey Tenant', 'Tenant', 'stenant1', 'Assistant Professor', 1, 'GvpBtSMpa', 1040530435, '7693367608', false, true, 0273400134, 77, 2262125279, 4);
insert into Users (authId, name, fullName, surname, secondName, status, companyUnitId, password, lastLoginTimestamp, iin, isActive, isActivated, createdTimestamp, createdBy, updatedTimestamp, updatedBy) values (4, 'Marlene', 'Marlene Lockyear', 'Lockyear', 'mlockyear2', 'Developer II', 3, 'K1EMuiHcoHV', 9753116837, '3123078434', false, false, 2089864303, 6926, 2180418051, 024);
insert into Users (authId, name, fullName, surname, secondName, status, companyUnitId, password, lastLoginTimestamp, iin, isActive, isActivated, createdTimestamp, createdBy, updatedTimestamp, updatedBy) values (2, 'Norene', 'Norene Piquard', 'Piquard', 'npiquard3', 'Sales Representative', 5, '7h46yOgt', 0414658876, '7841931123', true, true, 4945889473, 22, 4313294139, 98074);
insert into Users (authId, name, fullName, surname, secondName, status, companyUnitId, password, lastLoginTimestamp, iin, isActive, isActivated, createdTimestamp, createdBy, updatedTimestamp, updatedBy) values (1, 'Idette', 'Idette Lowdiane', 'Lowdiane', 'ilowdiane4', 'Information Systems Manager', 4, '1tbMwFOydHp', 2560319659, '0632813571', true, false, 6773829305, 681, 7943109880, 3);

insert into Share (requestId, note, senderId, receiverId, shareTimestamp) values (1, 'Re-engineered', 9770, 5, 7774833498);
insert into Share (requestId, note, senderId, receiverId, shareTimestamp) values (5, 'customer loyalty', 9264, 00668, 4216055834);
insert into Share (requestId, note, senderId, receiverId, shareTimestamp) values (1, 'alliance', 3202, 07395, 8786300369);
insert into Share (requestId, note, senderId, receiverId, shareTimestamp) values (2, 'Robust', 1214, 62, 3551267510);
insert into Share (requestId, note, senderId, receiverId, shareTimestamp) values (2, 'challenge', 21, 4, 5120132324);

insert into Request (requestUserId, responseUserId, caseId, caseIndexId, createdType, comment, status, timestamp, shareStart, shareFinish, favorite, updateTimestamp, updateBy, declineNote, companyUnitId, fromRequestId) values (0830, 19012, 1, 1, 'application/x-mspowerpoint', 'incremental', 'Information Systems Manager', 1452722382, 9672376189, 7336123252, true, 6119007938, 7617, 'transitional', 4, 470);
insert into Request (requestUserId, responseUserId, caseId, caseIndexId, createdType, comment, status, timestamp, shareStart, shareFinish, favorite, updateTimestamp, updateBy, declineNote, companyUnitId, fromRequestId) values (5876, 12, 3, 2, 'application/msword', 'help-desk', 'Social Worker', 7362602073, 8312946519, 3445370656, false, 1274613434, 152, 'Open-architected', 2, 4);
insert into Request (requestUserId, responseUserId, caseId, caseIndexId, createdType, comment, status, timestamp, shareStart, shareFinish, favorite, updateTimestamp, updateBy, declineNote, companyUnitId, fromRequestId) values (01, 71, 3, 5, 'application/vnd.ms-excel', 'migration', 'Quality Control Specialist', 5353367006, 9029527684, 0042500753, false, 3861284367, 16, 'Fully-configurable', 1, 90722);
insert into Request (requestUserId, responseUserId, caseId, caseIndexId, createdType, comment, status, timestamp, shareStart, shareFinish, favorite, updateTimestamp, updateBy, declineNote, companyUnitId, fromRequestId) values (63, 941, 2, 1, 'application/x-mspowerpoint', 'Programmable', 'Senior Quality Engineer', 5506348193, 8041907679, 1295682419, true, 5415283579, 9, 'Devolved', 4, 70488);
insert into Request (requestUserId, responseUserId, caseId, caseIndexId, createdType, comment, status, timestamp, shareStart, shareFinish, favorite, updateTimestamp, updateBy, declineNote, companyUnitId, fromRequestId) values (1, 0, 4, 5, 'application/vnd.ms-excel', 'client-driven', 'Biostatistician I', 5820344391, 4627341350, 3372218379, true, 2792027223, '13', 'Focused', 5, 5);

insert into HistoryRequestStatus (requestId, status, createdTimestamp, createdBy, updatedTimestamp, updatedBy) values (1, 'Programmer IV', 6611973265, 4, 6478229726, 849);
insert into HistoryRequestStatus (requestId, status, createdTimestamp, createdBy, updatedTimestamp, updatedBy) values (2, 'Assistant Media Planner', 3405006325, 896, 3722093198, 63);
insert into HistoryRequestStatus (requestId, status, createdTimestamp, createdBy, updatedTimestamp, updatedBy) values (3, 'Tax Accountant', 3692676417, 1, 3160168735, 155);
insert into HistoryRequestStatus (requestId, status, createdTimestamp, createdBy, updatedTimestamp, updatedBy) values (4, 'Structural Analysis Engineer', 4547100454, 707, 7911551294, 64298);
insert into HistoryRequestStatus (requestId, status, createdTimestamp, createdBy, updatedTimestamp, updatedBy) values (5, 'Senior Quality Engineer', 2144694818, 45321, 5957032069, 0365);

insert into CatalogCase (caseId, catalogId, companyUnitId, createdTimestamp, createdBy, updatedTimestamp, updatedBy) values (1, 1, 1, 2201380198, 84, 8566888553, 0523);
insert into CatalogCase (caseId, catalogId, companyUnitId, createdTimestamp, createdBy, updatedTimestamp, updatedBy) values (5, 2, 4, 8496285022, 57855, 3410992510, 72);
insert into CatalogCase (caseId, catalogId, companyUnitId, createdTimestamp, createdBy, updatedTimestamp, updatedBy) values (1, 1, 3, 2326039238, 712, 0624547787, 51910);
insert into CatalogCase (caseId, catalogId, companyUnitId, createdTimestamp, createdBy, updatedTimestamp, updatedBy) values (4, 2, 1, 4610140543, 56, 5590264057, 7);
insert into CatalogCase (caseId, catalogId, companyUnitId, createdTimestamp, createdBy, updatedTimestamp, updatedBy) values (2, 4, 3, 9812785965, 131, 4906762638, 1);

insert into Catalog (nameRu, nameKz, nameEn, parentId, companyUnitId, createdTimestamp, createdBy, updatedTimestamp, updatedBy) values ('Oozz', 'Snaptags', 'Demizz', 123, 3, 5844186877, 5, 4648543459, 15);
insert into Catalog (nameRu, nameKz, nameEn, parentId, companyUnitId, createdTimestamp, createdBy, updatedTimestamp, updatedBy) values ('Centimia', 'Skilith', 'Nlounge', 4534, 3, 7145925060, 5375, 4184862446, 03);
insert into Catalog (nameRu, nameKz, nameEn, parentId, companyUnitId, createdTimestamp, createdBy, updatedTimestamp, updatedBy) values ('Eabox', 'Tambee', 'Dynazzy', 4452, 4, 0239391012, 42, 5898211098, 29280);
insert into Catalog (nameRu, nameKz, nameEn, parentId, companyUnitId, createdTimestamp, createdBy, updatedTimestamp, updatedBy) values ('Blogtag', 'Dynabox', 'Tagtune', 963, 1, 3901644989, 6, 8148536155, 3133);
insert into Catalog (nameRu, nameKz, nameEn, parentId, companyUnitId, createdTimestamp, createdBy, updatedTimestamp, updatedBy) values ('Browsedrive', 'Realbridge', 'Vimbo', 7514, 2, 0799330183, 0769, 5719459596, 254);

insert into ActivityJournal (eventType, objectType, objectId, createdTimestamp, createdBy, messageLevel, message) values ('Konklux', 'Viva', 2, 5902979142, 312, '5', 'solution');
insert into ActivityJournal (eventType, objectType, objectId, createdTimestamp, createdBy, messageLevel, message) values ('Flexidy', 'Tin', 4, 3413127403, 192, '32', 'functionalities');
insert into ActivityJournal (eventType, objectType, objectId, createdTimestamp, createdBy, messageLevel, message) values ('Veribet', 'Stringtough', 5, 8734576789, 259, '38', 'function');
insert into ActivityJournal (eventType, objectType, objectId, createdTimestamp, createdBy, messageLevel, message) values ('Gembucket', 'Cardify', 5, 2321147806, 283, '366', 'Universal');
insert into ActivityJournal (eventType, objectType, objectId, createdTimestamp, createdBy, messageLevel, message) values ('Zoolab', 'Stringtough', 2, 6656280528, 21, '93', 'implementation');

insert into Location (row, line, columnN, box, companyUnitId, createdTimestamp, createdBy, updatedTimestamp, updatedBy) values ('25812', '75709', '7451', '61734', 3, 3433588740, 292, 0504843249, 223);
insert into Location (row, line, columnN, box, companyUnitId, createdTimestamp, createdBy, updatedTimestamp, updatedBy) values ('73', '3009', '07', '982', 3, 4504626325, 329, 7704121529, 420);
insert into Location (row, line, columnN, box, companyUnitId, createdTimestamp, createdBy, updatedTimestamp, updatedBy) values ('5917', '913', '67540', '606', 2, 7278760925, 314, 3538321264, 394);
insert into Location (row, line, columnN, box, companyUnitId, createdTimestamp, createdBy, updatedTimestamp, updatedBy) values ('43', '5820', '8', '7119', 4, 0100884040, 205, 4843354344, 225);
insert into Location (row, line, columnN, box, companyUnitId, createdTimestamp, createdBy, updatedTimestamp, updatedBy) values ('52', '30', '93707', '1', 2, 0757911021, 100, 5342612160, 99);

insert into Notification (objectType, objectId, companyUnitId, userId, createdTimestamp, viewedTimestamp, isViewed, title, text, companyId) values ('maximized', 4, 5, 4, 0662027620, 3298449997, true, 'Lamotrigine', 'data-warehouse', 5);
insert into Notification (objectType, objectId, companyUnitId, userId, createdTimestamp, viewedTimestamp, isViewed, title, text, companyId) values ('Total', 5, 3, 4, 3360675401, 6732439995, true, 'Paroxetine', 'homogeneous', 4);
insert into Notification (objectType, objectId, companyUnitId, userId, createdTimestamp, viewedTimestamp, isViewed, title, text, companyId) values ('data-warehouse', 4, 4, 1, 6936108695, 9359950726, false, 'CANDIDA ALBICANS', 'multi-state', 5);
insert into Notification (objectType, objectId, companyUnitId, userId, createdTimestamp, viewedTimestamp, isViewed, title, text, companyId) values ('algorithm', 5, 1, 3, 1470860880, 0416548962, true, 'Procainamide Hydrochloride', 'composite', 1);
insert into Notification (objectType, objectId, companyUnitId, userId, createdTimestamp, viewedTimestamp, isViewed, title, text, companyId) values ('methodology', 1, 4, 1, 2236985207, 8765432835, false, 'Citalopram', 'even-keeled', 4);

insert into CaseE (caseNumber, caseTom, caseHeadingRu, caseHeadingKz, caseHeadingEn, startDate, finishDate, pageNumber, Eds, edsSignature, sendingNaf, deletionSign, limitedAccess, hash, version, idVersion, activeVersion , note, locationId, caseIndexId, inventoryId, destructionActId, structuralSubdivisionId, caseBlockchainAddress, addBlockchainDate, createdTimestamp, createdBy, updatedTimestamp, updatedBy) values ('3924', '5', 'Dynava', 'Fanoodle', 'Jazzy', 7208136637, 8416929947, 54, true, 'system engine', true, true, false, '9614678194', 3, '1922526517', true, 'radical', 5, 1, 5, 2, 1, '1298011582', 4447034565, 8953011566, 459, 4342696568, 264);
insert into CaseE (caseNumber, caseTom, caseHeadingRu, caseHeadingKz, caseHeadingEn, startDate, finishDate, pageNumber, Eds, edsSignature, sendingNaf, deletionSign, limitedAccess, hash, version, idVersion, activeVersion , note, locationId, caseIndexId, inventoryId, destructionActId, structuralSubdivisionId, caseBlockchainAddress, addBlockchainDate, createdTimestamp, createdBy, updatedTimestamp, updatedBy) values ('4377', '7', 'Meeveo', 'Yotz', 'Thoughtsphere', 6359001918, 6609508682, 4, false, 'Switchable', true, true, false, '7079148030', 3, '4512398368', true, 'Optional', 3, 1, 5, 4, 1, '1523830700', 3657869727, 8277979067, 345, 7700162654, 400);
insert into CaseE (caseNumber, caseTom, caseHeadingRu, caseHeadingKz, caseHeadingEn, startDate, finishDate, pageNumber, Eds, edsSignature, sendingNaf, deletionSign, limitedAccess, hash, version, idVersion, activeVersion , note, locationId, caseIndexId, inventoryId, destructionActId, structuralSubdivisionId, caseBlockchainAddress, addBlockchainDate, createdTimestamp, createdBy, updatedTimestamp, updatedBy) values ('8', '7', 'Rhyzio', 'Linkbridge', 'Vipe', 5167225795, 2801527912, 1, false, 'Extended', false, true, false, '4473371859', 3, '1538538318', false, 'demand-driven', 2, 4, 1, 4, 2, '7461956663', 7283969284, 9855507495, 482, 4222601702, 187);
insert into CaseE (caseNumber, caseTom, caseHeadingRu, caseHeadingKz, caseHeadingEn, startDate, finishDate, pageNumber, Eds, edsSignature, sendingNaf, deletionSign, limitedAccess, hash, version, idVersion, activeVersion , note, locationId, caseIndexId, inventoryId, destructionActId, structuralSubdivisionId, caseBlockchainAddress, addBlockchainDate, createdTimestamp, createdBy, updatedTimestamp, updatedBy) values ('5', '3340', 'Jabbersphere', 'Abatz', 'Thoughtbeat', 9929634045, 4368955080, 4, false, 'collaboration', false, false, true, '2278910817', 4, '6890575369', true, 'content-based', 5, 1, 2, 4, 4, '3916169793', 7620885110, 0924311754, 176, 0559912293, 322);
insert into CaseE (caseNumber, caseTom, caseHeadingRu, caseHeadingKz, caseHeadingEn, startDate, finishDate, pageNumber, Eds, edsSignature, sendingNaf, deletionSign, limitedAccess, hash, version, idVersion, activeVersion , note, locationId, caseIndexId, inventoryId, destructionActId, structuralSubdivisionId, caseBlockchainAddress, addBlockchainDate, createdTimestamp, createdBy, updatedTimestamp, updatedBy) values ('624', '0', 'Lazz', 'Topdrive', 'Yadel', 7767382540, 1178631192, 13, false, 'Extended', false, true, true, '6616340333', 2, '2838479199', false, 'analyzing', 1, 3, 4, 1, 2, '7930925889', 9803425188, 6941085444, 417, 4721344856, 247);

insert into Nomenclature ( nomenclatureNumber, year, nomenclatureSummaryId, companyUnitId, createdTimestamp, createdBy, updatedTimestamp, updatedBy) values ('0874889634', 1996, 94, 1, 8598626635, 269, 4604420572, 52);
insert into Nomenclature ( nomenclatureNumber, year, nomenclatureSummaryId, companyUnitId, createdTimestamp, createdBy, updatedTimestamp, updatedBy) values ('5675295259', 2004, 56, 1, 6851143607, 187, 1722930667, 246);
insert into Nomenclature ( nomenclatureNumber, year, nomenclatureSummaryId, companyUnitId, createdTimestamp, createdBy, updatedTimestamp, updatedBy) values ('5657005724', 2006, 37, 3, 5684227287, 320, 1037542304, 177);
insert into Nomenclature ( nomenclatureNumber, year, nomenclatureSummaryId, companyUnitId, createdTimestamp, createdBy, updatedTimestamp, updatedBy) values ('7942541991', 1996, 84, 2, 0896820629, 73, 9108726612, 274);
insert into Nomenclature ( nomenclatureNumber, year, nomenclatureSummaryId, companyUnitId, createdTimestamp, createdBy, updatedTimestamp, updatedBy) values ('0830642021', 2010, 45, 4, 6636896329, 217, 2472592167, 259);

insert into CaseIndex (caseIndex, titleRu, titleKz, titleEn, storageType, storageYear, note, companyUnitId, nomenclatureId, createdTimestamp, createdBy, updatedTimestamp, updatedBy) values ('9857207553', 'acquirethisname.com', 'phpbb.com', 'sohu.com', 1, 1994, 'array', 5, 5, 6197861496, 434, 9131696589, 347);
insert into CaseIndex (caseIndex, titleRu, titleKz, titleEn, storageType, storageYear, note, companyUnitId, nomenclatureId, createdTimestamp, createdBy, updatedTimestamp, updatedBy) values ('4099193977', 'boston.com', 'stumbleupon.com', 'mayoclinic.com', 10, 2005, 'ability', 5, 1, 4718439256, 415, 8232752173, 63);
insert into CaseIndex (caseIndex, titleRu, titleKz, titleEn, storageType, storageYear, note, companyUnitId, nomenclatureId, createdTimestamp, createdBy, updatedTimestamp, updatedBy) values ('0921738412', 'geocities.com', 'epa.gov', '51.la', 10, 2006, 'composite', 3, 5, 0175360944, 500, 5350385046, 155);
insert into CaseIndex (caseIndex, titleRu, titleKz, titleEn, storageType, storageYear, note, companyUnitId, nomenclatureId, createdTimestamp, createdBy, updatedTimestamp, updatedBy) values ('2741357056', 'squidoo.com', 'vimeo.com', 'unicef.org', 8, 2001, 'Horizontal', 1, 1, 9258406036, 327, 8806658255, 112);
insert into CaseIndex (caseIndex, titleRu, titleKz, titleEn, storageType, storageYear, note, companyUnitId, nomenclatureId, createdTimestamp, createdBy, updatedTimestamp, updatedBy) values ('2745333720', 'phoca.cz', 'wisc.edu', 'marriott.com', 9, 1988, 'multimedia', 1, 2, 4399937890, 486, 9838229288, 331);

insert into Record (number, type, companyUnitId, createdTimestamp, createdBy, updatedTimestamp, updatedBy) values ('5114137425', 'image/gif', 3, 7186557295, 101, 7995109857, 68);
insert into Record (number, type, companyUnitId, createdTimestamp, createdBy, updatedTimestamp, updatedBy) values ('0900781785', 'image/pjpeg', 4, 6173988291, 106, 7257952566, 385);
insert into Record (number, type, companyUnitId, createdTimestamp, createdBy, updatedTimestamp, updatedBy) values ('5002338429', 'application/excel', 1, 3481613253, 10, 4663997406, 285);
insert into Record (number, type, companyUnitId, createdTimestamp, createdBy, updatedTimestamp, updatedBy) values ('1867407345', 'image/jpeg', 1, 1068739819, 422, 2156529906, 95);
insert into Record (number, type, companyUnitId, createdTimestamp, createdBy, updatedTimestamp, updatedBy) values ('2587453070', 'image/pjpeg', 4, 5925621743, 106, 9478761048, 349);

insert into NomenclatureSummary (number, year, companyUnitId, createdTimestamp, createdBy, updatedTimestamp, updatedBy) values ('7229078237', 1996, 4, 2447093519, 61, 0734096968, 291);
insert into NomenclatureSummary (number, year, companyUnitId, createdTimestamp, createdBy, updatedTimestamp, updatedBy) values ('5016151555', 2012, 3, 2882234570, 181, 8786326031, 469);
insert into NomenclatureSummary (number, year, companyUnitId, createdTimestamp, createdBy, updatedTimestamp, updatedBy) values ('9379928572', 1996, 2, 2927582963, 196, 3386279953, 96);
insert into NomenclatureSummary (number, year, companyUnitId, createdTimestamp, createdBy, updatedTimestamp, updatedBy) values ('6588005806', 1999, 5, 3753255319, 491, 2821078102, 53);
insert into NomenclatureSummary (number, year, companyUnitId, createdTimestamp, createdBy, updatedTimestamp, updatedBy) values ('6771967557', 1995, 2, 0863852602, 456, 0943525934, 144);

insert into DestructionAct (actNumber, base, structuralSubdivisionId, createdTimestamp, createdBy, updatedTimestamp, updatedBy) values ('1813173249', 'Distributed', 1, 8670199246, 199, 3187088015, 489);
insert into DestructionAct (actNumber, base, structuralSubdivisionId, createdTimestamp, createdBy, updatedTimestamp, updatedBy) values ('5551537390', 'Versatile', 2, 1807450309, 417, 8066022336, 425);
insert into DestructionAct (actNumber, base, structuralSubdivisionId, createdTimestamp, createdBy, updatedTimestamp, updatedBy) values ('0094650829', '6th generation', 4, 7546061067, 165, 9469572793, 404);
insert into DestructionAct (actNumber, base, structuralSubdivisionId, createdTimestamp, createdBy, updatedTimestamp, updatedBy) values ('2012585817', 'full-range', 3, 6428183080, 284, 7694662875, 198);
insert into DestructionAct (actNumber, base, structuralSubdivisionId, createdTimestamp, createdBy, updatedTimestamp, updatedBy) values ('8169788102', 'complexity', 3, 8309233558, 167, 6641862720, 103);

insert into File (name, type, size, pageCount, hash, isDeleted, fileBinaryId, createdTimestamp, createdBy, updatedTimestamp, updatedBy) values ('Y-Solowarm', 'bing.com', 391, 68, '5149949957', true, 3, 4886990673, 496, 6546854694, 240);
insert into File (name, type, size, pageCount, hash, isDeleted, fileBinaryId, createdTimestamp, createdBy, updatedTimestamp, updatedBy) values ('Alphazap', 'businessweek.com', 146, 78, '6578476415', true, 2, 2229103121, 461, 5549780771, 209);
insert into File (name, type, size, pageCount, hash, isDeleted, fileBinaryId, createdTimestamp, createdBy, updatedTimestamp, updatedBy) values ('Tresom', 'amazon.com', 338, 57, '4816075100', false, 5, 9020509454, 342, 4952295117, 194);
insert into File (name, type, size, pageCount, hash, isDeleted, fileBinaryId, createdTimestamp, createdBy, updatedTimestamp, updatedBy) values ('Alpha', 'scientificamerican.com', 275, 72, '5873179530', true, 4, 1899861564, 146, 0209444622, 327);
insert into File (name, type, size, pageCount, hash, isDeleted, fileBinaryId, createdTimestamp, createdBy, updatedTimestamp, updatedBy) values ('Veribet', 'jiathis.com', 224, 79, '1819188779', false, 5, 0239404289, 378, 9079889598, 167);

insert into FileRouting (fileId, tableName, tableId, type) values (2, 'canalblog.com', 2, 'video/msvideo');
insert into FileRouting (fileId, tableName, tableId, type) values (1, 'cdc.gov', 1, 'audio/x-mpeg-3');
insert into FileRouting (fileId, tableName, tableId, type) values (1, 'marriott.com', 4, 'image/jpeg');
insert into FileRouting (fileId, tableName, tableId, type) values (1, 'narod.ru', 4, 'application/vnd.ms-excel');
insert into FileRouting (fileId, tableName, tableId, type) values (4, 'parallels.com', 1, 'application/mspowerpoint');

insert into SearchKey (name, companyUnitId, createdTimestamp, createdBy, updatedTimestamp, updatedBy) values ('Gleostine', 3, 6038926224, 72, 4056990573, 62);
insert into SearchKey (name, companyUnitId, createdTimestamp, createdBy, updatedTimestamp, updatedBy) values ('Hydroxyzine Hydrochloride', 5, 4488465412, 56, 2985486025, 87);
insert into SearchKey (name, companyUnitId, createdTimestamp, createdBy, updatedTimestamp, updatedBy) values ('carbidopa, levodopa and entacapone', 3, 9086523781, 52, 7032539696, 89);
insert into SearchKey (name, companyUnitId, createdTimestamp, createdBy, updatedTimestamp, updatedBy) values ('Treatment Set TS344001', 3, 5294759535, 73, 4269499176, 50);
insert into SearchKey (name, companyUnitId, createdTimestamp, createdBy, updatedTimestamp, updatedBy) values ('Metformin Hydrochloride', 2, 7214357003, 89, 4001396149, 87);

insert into SearchKeyRouting (searchKeyId, tableName, tableId, type) values (1, 'time-frame', 4, 'application/msword');
insert into SearchKeyRouting (searchKeyId, tableName, tableId, type) values (5, 'fresh-thinking', 1, 'text/plain');
insert into SearchKeyRouting (searchKeyId, tableName, tableId, type) values (5, 'eco-centric', 4, 'audio/x-mpeg-3');
insert into SearchKeyRouting (searchKeyId, tableName, tableId, type) values (3, 'moratorium', 2, 'image/x-tiff');
insert into SearchKeyRouting (searchKeyId, tableName, tableId, type) values (5, 'emulation', 4, 'video/quicktime');

insert into TempFiles (fileBinary, fileBinaryByte) values ('project', 2);
insert into TempFiles (fileBinary, fileBinaryByte) values ('fresh-thinking', 1);
insert into TempFiles (fileBinary, fileBinaryByte) values ('Visionary', 2);
insert into TempFiles (fileBinary, fileBinaryByte) values ('middleware', 3);
insert into TempFiles (fileBinary, fileBinaryByte) values ('Object-based', 3);





















