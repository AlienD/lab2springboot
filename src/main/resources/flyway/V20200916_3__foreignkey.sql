ALTER TABLE Users
    ADD CONSTRAINT fk1 FOREIGN KEY (authId) REFERENCES AuthorizationN(id);

ALTER TABLE companyunit
    ADD CONSTRAINT fk1 FOREIGN KEY (companyid) REFERENCES company(id);

ALTER TABLE company
    ADD CONSTRAINT fk1 FOREIGN KEY (fondid) REFERENCES fond(id);

ALTER TABLE record
    ADD CONSTRAINT fk1 FOREIGN KEY (companyunitid) REFERENCES companyunit(id);

ALTER TABLE share
    ADD CONSTRAINT fk1 FOREIGN KEY (requestid) REFERENCES request(id);

ALTER TABLE historyrequeststatus
    ADD CONSTRAINT fk1 FOREIGN KEY (requestid) REFERENCES request(id);

ALTER TABLE catalogcase
    ADD CONSTRAINT fk1 FOREIGN KEY (catalogid) REFERENCES catalog(id);

ALTER TABLE casee
    ADD CONSTRAINT fk1 FOREIGN KEY (locationid) REFERENCES location(id);

ALTER TABLE catalogcase
    ADD CONSTRAINT fk2 FOREIGN KEY (caseid) REFERENCES casee(id);

ALTER TABLE catalogcase
    ADD CONSTRAINT fk3 FOREIGN KEY (companyunitid) REFERENCES companyunit(id);

ALTER TABLE filerouting
    ADD CONSTRAINT fk1 FOREIGN KEY (tableid) REFERENCES casee(id);

ALTER TABLE request
    ADD CONSTRAINT fk1 FOREIGN KEY (caseid) REFERENCES casee(id);

ALTER TABLE request
    ADD CONSTRAINT fk2 FOREIGN KEY (caseindexid) REFERENCES caseindex(id);

ALTER TABLE filerouting
    ADD CONSTRAINT fk2 FOREIGN KEY (fileid) REFERENCES file(id);

ALTER TABLE file
    ADD CONSTRAINT fk1 FOREIGN KEY (filebinaryid) REFERENCES tempfiles(id);

ALTER TABLE casee
    ADD CONSTRAINT fk2 FOREIGN KEY (structuralsubdivisionid) REFERENCES companyunit(id);

ALTER TABLE casee
    ADD CONSTRAINT fk3 FOREIGN KEY (destructionactid) REFERENCES destructionact(id);

ALTER TABLE casee
    ADD CONSTRAINT fk4 FOREIGN KEY (inventoryid) REFERENCES record(id);

ALTER TABLE casee
    ADD CONSTRAINT fk5 FOREIGN KEY (caseindexid) REFERENCES caseindex(id);

ALTER TABLE nomenclaturesummary
    ADD CONSTRAINT fk1 FOREIGN KEY (companyunitid) REFERENCES companyunit(id);

ALTER TABLE users
    ADD CONSTRAINT fk2 FOREIGN KEY (companyunitid) REFERENCES companyunit(id);

ALTER TABLE request
    ADD CONSTRAINT fk3 FOREIGN KEY (companyunitid) REFERENCES companyunit(id);

ALTER TABLE nomenclature
    ADD CONSTRAINT fk1 FOREIGN KEY (companyunitid) REFERENCES companyunit(id);

ALTER TABLE searchkey
    ADD CONSTRAINT fk1 FOREIGN KEY (companyunitid) REFERENCES companyunit(id);

ALTER TABLE searchkeyrouting
    ADD CONSTRAINT fk1 FOREIGN KEY (searchkeyid) REFERENCES searchkey(id);

ALTER TABLE searchkeyrouting
    ADD CONSTRAINT fk2 FOREIGN KEY (tableid) REFERENCES casee(id);

ALTER TABLE destructionact
    ADD CONSTRAINT fk1 FOREIGN KEY (structuralsubdivisionid) REFERENCES companyunit(id);

ALTER TABLE caseindex
    ADD CONSTRAINT fk1 FOREIGN KEY (companyunitid) REFERENCES companyunit(id);